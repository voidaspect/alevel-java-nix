create table locations
(
    id serial not null
        constraint locations_pk
            primary key,
    name varchar(255) not null
);

create unique index locations_name_uindex
    on locations (name);

create table connections
(
    id serial not null
        constraint connections_pk
            primary key,
    from_id integer not null
        constraint connections_locations_from_id_fk
            references locations,
    to_id integer not null
        constraint connections_locations_to_id_fk
            references locations,
    cost integer not null
);

create table problems
(
    id serial not null
        constraint problems_pk
            primary key,
    from_id integer not null
        constraint problems_locations_from_id_fk
            references locations,
    to_id integer not null
        constraint problems_locations_to_id_fk
            references locations
);

create table solutions
(
    problem_id integer not null
        constraint solutions_pk
            primary key
        constraint solutions_problems_id_fk
            references problems,
    cost integer
);

