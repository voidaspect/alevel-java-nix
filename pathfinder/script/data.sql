TRUNCATE TABLE solutions, problems, connections, locations RESTART IDENTITY CASCADE ;

INSERT INTO locations (name) VALUES ('gdansk');
INSERT INTO locations (name) VALUES ('bydgoszcz');
INSERT INTO locations (name) VALUES ('torun');
INSERT INTO locations (name) VALUES ('warszawa');
INSERT INTO locations (name) VALUES ('kharkiv');

INSERT INTO connections (from_id, to_id, cost) VALUES (1, 2, 1);
INSERT INTO connections (from_id, to_id, cost) VALUES (1, 3, 3);
INSERT INTO connections (from_id, to_id, cost) VALUES (2, 1, 1);
INSERT INTO connections (from_id, to_id, cost) VALUES (2, 3, 1);
INSERT INTO connections (from_id, to_id, cost) VALUES (2, 4, 4);
INSERT INTO connections (from_id, to_id, cost) VALUES (3, 1, 3);
INSERT INTO connections (from_id, to_id, cost) VALUES (3, 2, 1);
INSERT INTO connections (from_id, to_id, cost) VALUES (3, 4, 1);
INSERT INTO connections (from_id, to_id, cost) VALUES (4, 2, 4);
INSERT INTO connections (from_id, to_id, cost) VALUES (4, 3, 1);

INSERT INTO problems (from_id, to_id) VALUES (1, 4);
INSERT INTO problems (from_id, to_id) VALUES (1, 3);
INSERT INTO problems (from_id, to_id) VALUES (2, 4);
INSERT INTO problems (from_id, to_id) VALUES (2, 5);