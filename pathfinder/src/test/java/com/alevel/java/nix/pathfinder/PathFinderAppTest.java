package com.alevel.java.nix.pathfinder;

import com.alevel.java.nix.pathfinder.io.ConfigParser;
import com.alevel.java.nix.pathfinder.model.Problem;
import com.alevel.java.nix.pathfinder.model.ProblemBuilder;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

class PathFinderAppTest {

    @Test
    void shouldFindBestPaths() {
        var problem = new ProblemBuilder(3)
                .name(0, "a")
                .name(1, "b")
                .name(2, "c")
                .connect("a", "b", 10)
                .connect("a", "c", 20)
                .connectBiDirectional("b", "c", 5)
                .solve(1, "a", "c")
                .solve(2, "b", "c")
                .solve(3, "c", "a")
                .solve(4, 2, 1)
                .build();

        assertEquals(3, problem.size());
        assertEquals("a", problem.vertex(0).getName());
        assertEquals(0, problem.vertex(0).getIndex());
        assertEquals("b", problem.vertex(1).getName());
        assertEquals(1, problem.vertex(1).getIndex());
        assertEquals("c", problem.vertex(2).getName());
        assertEquals(2, problem.vertex(2).getIndex());

        var solutions = problem.solve();

        assertEquals(4, solutions.size());

        var ac = solutions.get(0);
        assertEquals("a", ac.getFrom().getName());
        assertEquals("c", ac.getTo().getName());
        assertRouteFound(15, ac);

        var bc = solutions.get(1);
        assertEquals("b", bc.getFrom().getName());
        assertEquals("c", bc.getTo().getName());
        assertRouteFound(5, bc);

        var ca = solutions.get(2);
        assertEquals("c", ca.getFrom().getName());
        assertEquals("a", ca.getTo().getName());
        assertRouteNotFound(ca);

        var cb = solutions.get(3);
        assertEquals("c", cb.getFrom().getName());
        assertEquals("b", cb.getTo().getName());
        assertRouteFound(5, bc);
    }

    @Test
    void shouldSolveProblemFromFile() {
        Problem problem;

        try(var parser = new ConfigParser(new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/input.txt"))))) {
            problem = assertDoesNotThrow(parser::load);
        }

        assertEquals(5, problem.size());
        assertEquals("gdansk", problem.vertex(0).getName());
        assertEquals("bydgoszcz", problem.vertex(1).getName());
        assertEquals("torun", problem.vertex(2).getName());
        assertEquals("warszawa", problem.vertex(3).getName());
        assertEquals("kharkiv", problem.vertex(4).getName());

        var solutions = problem.solve();
        assertEquals(4, solutions.size());

        var solution = solutions.get(0);
        assertEquals("gdansk", solution.getFrom().getName());
        assertEquals("warszawa", solution.getTo().getName());
        assertRouteFound(3, solution);

        solution = solutions.get(1);
        assertEquals("gdansk", solution.getFrom().getName());
        assertEquals("torun", solution.getTo().getName());
        assertRouteFound(2, solution);

        solution = solutions.get(2);
        assertEquals("bydgoszcz", solution.getFrom().getName());
        assertEquals("warszawa", solution.getTo().getName());
        assertRouteFound(2, solution);

        solution = solutions.get(3);
        assertEquals("bydgoszcz", solution.getFrom().getName());
        assertEquals("kharkiv", solution.getTo().getName());
        assertRouteNotFound(solution);
    }

    @Test
    void shouldNotAllowInvalidConfig() {
        assertThrows(IllegalArgumentException.class, () -> new ProblemBuilder(2)
                .name(0, "a")
                .name(1, "a"));
        assertThrows(IllegalArgumentException.class, () -> new ProblemBuilder(2)
                .name(0, "a")
                .name(1, "b")
                .connect(0, 1, -1));
        assertThrows(IllegalArgumentException.class, () -> new ProblemBuilder(2)
                .name(0, "a")
                .name(1, "b")
                .connect(0, 1, 0));
    }

    private static void assertRouteFound(int distance, Problem.Solution solution) {
        assertTrue(solution instanceof Problem.RouteFound);
        assertEquals(distance, ((Problem.RouteFound) solution).getDistance());
        assertEquals(String.valueOf(distance), solution.asText());
    }

    private static void assertRouteNotFound(Problem.Solution solution) {
        assertTrue(solution instanceof Problem.RouteNotFound);
        assertEquals("route not found", solution.asText());
    }
}