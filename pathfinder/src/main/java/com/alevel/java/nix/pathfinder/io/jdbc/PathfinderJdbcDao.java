package com.alevel.java.nix.pathfinder.io.jdbc;

import com.alevel.java.nix.pathfinder.io.ConfigSource;
import com.alevel.java.nix.pathfinder.io.SolutionConsumer;
import com.alevel.java.nix.pathfinder.model.Problem;
import com.alevel.java.nix.pathfinder.model.ProblemBuilder;

import java.sql.*;
import java.util.*;
import java.util.function.Supplier;

public class PathfinderJdbcDao implements ConfigSource, SolutionConsumer {

    private final Supplier<Connection> connectionPool;

    public PathfinderJdbcDao(Supplier<Connection> connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public Problem load() {

        Map<Integer, Integer> locationIdToIndex = new HashMap<>();
        List<String> locationNames = new ArrayList<>();
        int locationNumber = 0;

        Connection connection = connectionPool.get();

        try(PreparedStatement fetchLocations = connection.prepareStatement("SELECT * FROM locations")) {
            var resultSet = fetchLocations.executeQuery();
            while (resultSet.next()) {
                locationIdToIndex.put(resultSet.getInt("id"), locationNumber);
                locationNames.add(resultSet.getString("name"));
                locationNumber++;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        final var problemBuilder = new ProblemBuilder(locationNumber);

        for (int i = 0; i < locationNumber; i++) {
            problemBuilder.name(i, locationNames.get(i));
        }

        try(PreparedStatement fetchConnections = connection.prepareStatement(
                "SELECT from_id, to_id, cost FROM connections")) {
            var resultSet = fetchConnections.executeQuery();
            while (resultSet.next()) {
                int fromIndex = locationIdToIndex.get(resultSet.getInt("from_id"));
                int toIndex = locationIdToIndex.get(resultSet.getInt("to_id"));
                problemBuilder.connect(fromIndex, toIndex, resultSet.getInt("cost"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try(PreparedStatement fetchProblems = connection.prepareStatement(
                "SELECT p.id, p.from_id, p.to_id FROM problems p " +
                        "LEFT JOIN solutions s ON p.id = s.problem_id " +
                        "WHERE s.problem_id IS NULL")) {

            var resultSet = fetchProblems.executeQuery();
            while (resultSet.next()) {
                problemBuilder.solve(
                        resultSet.getInt("id"),
                        locationIdToIndex.get(resultSet.getInt("from_id")),
                        locationIdToIndex.get(resultSet.getInt("to_id"))
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return problemBuilder.build();
    }

    @Override
    public void consume(Collection<Problem.Solution> solutions) {
        String sql = "INSERT INTO solutions (problem_id, cost) VALUES (?, ?)";

        try(PreparedStatement update = connectionPool.get().prepareStatement(sql)) {

            for (Problem.Solution solution : solutions) {
                update.setInt(1, solution.getProblemId());

                if (solution instanceof Problem.RouteFound) {
                    update.setInt(2, ((Problem.RouteFound) solution).getDistance());
                } else {
                    update.setNull(2, Types.INTEGER);
                }

                update.addBatch();
            }

            update.executeBatch();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
