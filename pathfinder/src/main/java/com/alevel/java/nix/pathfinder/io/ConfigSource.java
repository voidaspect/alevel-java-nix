package com.alevel.java.nix.pathfinder.io;

import com.alevel.java.nix.pathfinder.model.Problem;

public interface ConfigSource {

    Problem load();

}
