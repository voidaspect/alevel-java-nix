package com.alevel.java.nix.pathfinder.io;

import com.alevel.java.nix.pathfinder.model.Problem;

import java.util.Collection;
import java.util.stream.Collectors;

abstract class SolutionTextualConsumer implements SolutionConsumer {

    @Override
    public void consume(Collection<Problem.Solution> solutions) {
        var text = solutions.stream()
                .map(Problem.Solution::asText)
                .collect(Collectors.joining(System.lineSeparator()));
        consume(text);
    }

    abstract void consume(String text);
}
