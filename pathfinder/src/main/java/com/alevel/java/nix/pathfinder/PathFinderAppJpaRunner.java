package com.alevel.java.nix.pathfinder;

import com.alevel.java.nix.pathfinder.io.jpa.PathFinderJpaDao;
import com.alevel.java.nix.pathfinder.model.Problem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class PathFinderAppJpaRunner {

    public static void main(String[] args) {

        Configuration cfg = new Configuration().configure();

        try(SessionFactory sf = cfg.buildSessionFactory();
            Session session = sf.openSession()) {

            var dao = new PathFinderJpaDao(() -> session);

            Problem problem = dao.load();

            List<Problem.Solution> solutions = problem.solve();

            dao.consume(solutions);
        }

    }
}
