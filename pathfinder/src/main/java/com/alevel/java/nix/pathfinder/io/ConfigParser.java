package com.alevel.java.nix.pathfinder.io;

import com.alevel.java.nix.pathfinder.model.Problem;
import com.alevel.java.nix.pathfinder.model.ProblemBuilder;

import java.io.Closeable;
import java.io.Reader;
import java.util.Scanner;

public class ConfigParser implements Closeable, ConfigSource {

    private final Scanner scanner;

    public ConfigParser(Reader source) {
        this.scanner = new Scanner(source);
    }

    @Override
    public void close() {
        scanner.close();
    }

    @Override
    public Problem load() {
        int size = scanner.nextInt();
        scanner.nextLine();

        var builder = new ProblemBuilder(size);

        for (int vertex = 0; vertex < size; vertex++) {
            builder.name(vertex, scanner.nextLine());
            int neighbors = scanner.nextInt();
            scanner.nextLine();
            for (int neighbor = 0; neighbor < neighbors; neighbor++) {
                builder.connect(vertex, scanner.nextInt() - 1, scanner.nextInt());
                scanner.nextLine();
            }
        }

        int numberOfRoutesToCalculate = scanner.nextInt();

        for (int route = 0; route < numberOfRoutesToCalculate; route++) {
            builder.solve(route + 1, scanner.next(), scanner.next());
            scanner.nextLine();
        }

        return builder.build();
    }
}
