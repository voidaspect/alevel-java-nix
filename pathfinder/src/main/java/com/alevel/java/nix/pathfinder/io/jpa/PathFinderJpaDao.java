package com.alevel.java.nix.pathfinder.io.jpa;

import com.alevel.java.nix.pathfinder.io.jpa.entity.Connection;
import com.alevel.java.nix.pathfinder.io.jpa.entity.Location;
import com.alevel.java.nix.pathfinder.io.jpa.entity.Solution;
import com.alevel.java.nix.pathfinder.io.jpa.entity.SubProblem;
import com.alevel.java.nix.pathfinder.io.ConfigSource;
import com.alevel.java.nix.pathfinder.io.SolutionConsumer;
import com.alevel.java.nix.pathfinder.model.Problem;
import com.alevel.java.nix.pathfinder.model.ProblemBuilder;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

public class PathFinderJpaDao implements ConfigSource, SolutionConsumer {

    private final Supplier<Session> persistence;

    public PathFinderJpaDao(Supplier<Session> persistence) {
        this.persistence = persistence;
    }

    @Override
    public Problem load() {
        Session session = persistence.get();

        Transaction tx = session.beginTransaction();

        try {
            CriteriaBuilder cb = session.getCriteriaBuilder();

            CriteriaQuery<SubProblem> fetchUnsolvedProblems = cb.createQuery(SubProblem.class);

            Root<SubProblem> subProblemRoot = fetchUnsolvedProblems.from(SubProblem.class);

            fetchUnsolvedProblems.where(subProblemRoot.join("solution", JoinType.LEFT).isNull());

            List<SubProblem> unresolvedProblems = session.createQuery(fetchUnsolvedProblems).getResultList();

            if (unresolvedProblems.isEmpty()) {
                tx.commit();
                return new Problem(List.of(), List.of());
            }

            CriteriaQuery<Location> fetchAllLocations = cb.createQuery(Location.class);
            fetchAllLocations.from(Location.class);

            List<Location> locations = session.createQuery(fetchAllLocations).getResultList();

            int locationsSize = locations.size();

            ProblemBuilder pb = new ProblemBuilder(locationsSize);

            for (int i = 0; i < locationsSize; i++) {
                Location location = locations.get(i);
                pb.name(i, location.getName());
            }

            CriteriaQuery<Connection> fetchAllConnections = cb.createQuery(Connection.class);
            fetchAllConnections.from(Connection.class);

            List<Connection> connections = session.createQuery(fetchAllConnections).getResultList();

            for (Connection connection : connections) {
                pb.connect(
                        connection.getFrom().getName(),
                        connection.getTo().getName(),
                        connection.getCost()
                );
            }

            for (SubProblem problem : unresolvedProblems) {
                pb.solve(
                        problem.getId(),
                        problem.getFrom().getName(),
                        problem.getTo().getName()
                );
            }

            tx.commit();

            return pb.build();
        } catch (Exception e) {
            tx.rollback();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void consume(Collection<Problem.Solution> solutions) {
        Session session = persistence.get();
        Transaction tx = session.beginTransaction();
        try {
            for (Problem.Solution solution : solutions) {
                var entity = new Solution();
                int problemId = solution.getProblemId();
                entity.setProblemId(problemId);
                entity.setProblem(session.load(SubProblem.class, problemId));

                if (solution instanceof Problem.RouteFound) {
                    entity.setCost(((Problem.RouteFound) solution).getDistance());
                }
                session.save(entity);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw new RuntimeException(e);
        }
    }
}
