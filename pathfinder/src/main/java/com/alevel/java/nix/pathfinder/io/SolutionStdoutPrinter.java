package com.alevel.java.nix.pathfinder.io;

public class SolutionStdoutPrinter extends SolutionTextualConsumer {
    @Override
    void consume(String text) {
        System.out.println(text);
    }
}
