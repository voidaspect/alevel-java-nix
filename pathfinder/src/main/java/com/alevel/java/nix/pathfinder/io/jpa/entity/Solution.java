package com.alevel.java.nix.pathfinder.io.jpa.entity;

import javax.persistence.*;

@Entity
@Table(name = "solutions")
public class Solution {

    @Id
    @Column(name = "problem_id")
    private Integer problemId;

    @MapsId
    @OneToOne
    @JoinColumn(name = "problem_id")
    private SubProblem problem;

    private Integer cost;

    public boolean isPossible() {
        return cost != null;
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public SubProblem getProblem() {
        return problem;
    }

    public void setProblem(SubProblem problem) {
        this.problem = problem;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
