package com.alevel.java.nix.pathfinder.io;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class SolutionFilePrinter extends SolutionTextualConsumer {

    private final Path target;

    public SolutionFilePrinter(Path target) {
        this.target = Objects.requireNonNull(target);
    }

    @Override
    void consume(String text) {
        try {
            Files.writeString(target, text);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
