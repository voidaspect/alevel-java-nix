package com.alevel.java.nix.pathfinder.io;

import com.alevel.java.nix.pathfinder.model.Problem;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class SolutionConsumerChain implements SolutionConsumer {

    private final List<SolutionConsumer> consumers;

    public SolutionConsumerChain(SolutionConsumer... consumers) {
        this.consumers = Arrays.asList(consumers);
    }

    public SolutionConsumerChain(List<SolutionConsumer> consumers) {
        this.consumers = Objects.requireNonNull(consumers);
    }

    @Override
    public void consume(Collection<Problem.Solution> solutions) {
        for (var consumer : consumers) {
            consumer.consume(solutions);
        }
    }
}
