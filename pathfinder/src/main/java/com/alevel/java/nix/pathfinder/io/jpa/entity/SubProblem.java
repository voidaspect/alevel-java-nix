package com.alevel.java.nix.pathfinder.io.jpa.entity;

import javax.persistence.*;

@Entity
@Table(name = "problems")
public class SubProblem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "from_id", nullable = false)
    private Location from;

    @ManyToOne
    @JoinColumn(name = "to_id", nullable = false)
    private Location to;

    @OneToOne(mappedBy = "problem")
    private Solution solution;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Location getFrom() {
        return from;
    }

    public void setFrom(Location from) {
        this.from = from;
    }

    public Location getTo() {
        return to;
    }

    public void setTo(Location to) {
        this.to = to;
    }

    public Solution getSolution() {
        return solution;
    }

    public void setSolution(Solution solution) {
        this.solution = solution;
    }
}
