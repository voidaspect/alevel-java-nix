package com.alevel.java.nix.pathfinder.io.jpa.entity;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "locations")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NaturalId
    private String name;

    @OneToMany(mappedBy = "from")
    private final List<Connection> outgoing = new ArrayList<>();

    @OneToMany(mappedBy = "to")
    private final List<Connection> incoming = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Connection> getOutgoing() {
        return outgoing;
    }

    public List<Connection> getIncoming() {
        return incoming;
    }
}
