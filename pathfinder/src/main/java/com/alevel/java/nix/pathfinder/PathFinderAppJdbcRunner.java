package com.alevel.java.nix.pathfinder;

import com.alevel.java.nix.pathfinder.io.jdbc.PathfinderJdbcDao;
import com.alevel.java.nix.pathfinder.io.SolutionConsumer;
import com.alevel.java.nix.pathfinder.io.SolutionConsumerChain;
import com.alevel.java.nix.pathfinder.io.SolutionStdoutPrinter;
import com.alevel.java.nix.pathfinder.model.Problem;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class PathFinderAppJdbcRunner {
    public static void main(String[] args) {

        var jdbcProps = new Properties();

        try(InputStream input = PathFinderAppJdbcRunner.class.getResourceAsStream("/jdbc.properties")) {
            jdbcProps.load(input);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        try (Connection connection = DriverManager.getConnection(jdbcProps.getProperty("url"), jdbcProps)) {
            var dao = new PathfinderJdbcDao(() -> connection);

            Problem problem = dao.load();

            List<Problem.Solution> solutions = problem.solve();

            SolutionConsumer solutionConsumerChain = new SolutionConsumerChain(new SolutionStdoutPrinter(), dao);

            solutionConsumerChain.consume(solutions);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
