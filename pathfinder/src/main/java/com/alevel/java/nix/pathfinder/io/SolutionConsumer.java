package com.alevel.java.nix.pathfinder.io;

import com.alevel.java.nix.pathfinder.model.Problem;

import java.util.Collection;

public interface SolutionConsumer {

    void consume(Collection<Problem.Solution> solutions);

}
