package com.alevel.java.nix.todolist.exception;

import org.springframework.http.HttpStatus;

public class UsernameAlreadyTakenException extends TodolistException {
    public UsernameAlreadyTakenException(String username) {
        super(HttpStatus.BAD_REQUEST, "User " + username + " already exists");
    }
}
