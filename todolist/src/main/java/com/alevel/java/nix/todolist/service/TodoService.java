package com.alevel.java.nix.todolist.service;

import com.alevel.java.nix.todolist.entity.Todo;
import com.alevel.java.nix.todolist.entity.TodoStatus;
import com.alevel.java.nix.todolist.entity.request.SaveTodoRequest;
import com.alevel.java.nix.todolist.exception.TodoNotFoundException;
import com.alevel.java.nix.todolist.exception.UserNotFoundException;
import com.alevel.java.nix.todolist.repository.TodoRepository;
import com.alevel.java.nix.todolist.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class TodoService implements TodoOperations {

    private final TodoRepository todoRepository;

    private final UserRepository userRepository;

    public TodoService(TodoRepository todoRepository, UserRepository userRepository) {
        this.todoRepository = todoRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Todo create(Integer userId, SaveTodoRequest request) {
        var user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

        var todo = new Todo();
        todo.setUser(user);
        todo.setText(request.getText());
        todo.setStatus(request.getStatus());

        user.getTodos().add(todo);

        return todoRepository.save(todo);
    }

    @Override
    public void update(UUID id, SaveTodoRequest request) {
        var existingTodo = getById(id);
        existingTodo.setStatus(request.getStatus());
        existingTodo.setText(request.getText());
        todoRepository.save(existingTodo);
    }

    @Override
    public void updateStatus(UUID id, TodoStatus status) {
        var existingTodo = getById(id);
        existingTodo.setStatus(status);
        todoRepository.save(existingTodo);
    }

    @Override
    public Todo getById(UUID id) {
        return todoRepository.findById(id).orElseThrow(() -> new TodoNotFoundException(id));
    }

    @Override
    public void deleteById(UUID id) {
        todoRepository.deleteById(id);
    }

    @Override
    public List<Todo> getAllByUserIdAndStatus(Integer userId, TodoStatus status) {
        if (!userRepository.existsById(userId)) {
            throw new UserNotFoundException(userId);
        }
        return todoRepository.findAllByUser_IdAndStatus(userId, status);
    }
}
