package com.alevel.java.nix.todolist.entity;

public enum TodoStatus {
    READY,
    IN_PROGRESS,
    DONE
}
