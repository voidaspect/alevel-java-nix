package com.alevel.java.nix.todolist.controller;

import com.alevel.java.nix.todolist.entity.request.SaveTodoRequest;
import com.alevel.java.nix.todolist.entity.request.SetTodoStatusRequest;
import com.alevel.java.nix.todolist.entity.response.TodoResponse;
import com.alevel.java.nix.todolist.service.TodoOperations;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private final TodoOperations todoOperations;

    public TodoController(TodoOperations todoOperations) {
        this.todoOperations = todoOperations;
    }

    @GetMapping("/{id}")
    public TodoResponse getById(@PathVariable UUID id) {
        var todo = todoOperations.getById(id);
        return TodoResponse.fromTodo(todo);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable UUID id, @RequestBody SaveTodoRequest request) {
        todoOperations.update(id, request);
    }

    @PatchMapping("/{id}/status")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatus(@PathVariable UUID id, @RequestBody SetTodoStatusRequest request) {
        todoOperations.updateStatus(id, request.getStatus());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatus(@PathVariable UUID id) {
        todoOperations.deleteById(id);
    }
}
