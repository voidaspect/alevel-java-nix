package com.alevel.java.nix.todolist.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TodolistException extends ResponseStatusException {
    public TodolistException(HttpStatus status) {
        super(status);
    }

    public TodolistException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public TodolistException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
    }
}
