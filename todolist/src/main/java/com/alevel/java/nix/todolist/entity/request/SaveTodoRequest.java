package com.alevel.java.nix.todolist.entity.request;

import com.alevel.java.nix.todolist.entity.TodoStatus;

public class SaveTodoRequest {

    private String text;

    private TodoStatus status;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TodoStatus getStatus() {
        return status;
    }

    public void setStatus(TodoStatus status) {
        this.status = status;
    }
}
