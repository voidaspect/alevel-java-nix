package com.alevel.java.nix.todolist.exception;

import org.springframework.http.HttpStatus;

public class RoleNotFoundException extends TodolistException {

    public RoleNotFoundException(String name) {
        super(HttpStatus.NOT_FOUND, "Role " + name + "  not found");
    }
}
