package com.alevel.java.nix.todolist.repository;

import com.alevel.java.nix.todolist.entity.Todo;
import com.alevel.java.nix.todolist.entity.TodoStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TodoRepository extends JpaRepository<Todo, UUID> {

    List<Todo> findAllByUser_IdAndStatus(Integer userId, TodoStatus status);

}
