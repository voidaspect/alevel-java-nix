package com.alevel.java.nix.todolist.service;

import com.alevel.java.nix.todolist.entity.Todo;
import com.alevel.java.nix.todolist.entity.TodoStatus;
import com.alevel.java.nix.todolist.entity.request.SaveTodoRequest;

import java.util.List;
import java.util.UUID;

public interface TodoOperations {

    Todo create(Integer userId, SaveTodoRequest request);

    void update(UUID id, SaveTodoRequest request);

    void updateStatus(UUID id, TodoStatus status);

    Todo getById(UUID id);

    void deleteById(UUID id);

    List<Todo> getAllByUserIdAndStatus(Integer userId, TodoStatus status);

}
