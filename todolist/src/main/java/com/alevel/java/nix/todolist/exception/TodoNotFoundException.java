package com.alevel.java.nix.todolist.exception;

import org.springframework.http.HttpStatus;

import java.util.UUID;

public class TodoNotFoundException extends TodolistException {


    public TodoNotFoundException(UUID id) {
        super(HttpStatus.NOT_FOUND, "Todo item with id " + id + " not found");
    }
}
