package com.alevel.java.nix.todolist.service;

import com.alevel.java.nix.todolist.entity.User;
import com.alevel.java.nix.todolist.entity.request.SaveUserRequest;

public interface UserOperations {

    User create(SaveUserRequest request);

    void update(Integer id, SaveUserRequest request);

    User getById(Integer id);

    void deleteById(Integer id);

}
