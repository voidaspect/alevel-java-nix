package com.alevel.java.nix.todolist.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends TodolistException {

    public UserNotFoundException(Integer id) {
        super(HttpStatus.NOT_FOUND, "User with id " + id + "  not found");
    }

    public UserNotFoundException(String username) {
        super(HttpStatus.NOT_FOUND, "User " + username + "  not found");
    }

}
