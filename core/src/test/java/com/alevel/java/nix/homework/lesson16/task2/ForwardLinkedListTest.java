package com.alevel.java.nix.homework.lesson16.task2;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ForwardLinkedListTest {


    @Test
    void shouldAddElements() {
        var list = new ForwardLinkedList<String>();
        assertEquals(0, list.size());
        list.add("one");

        assertEquals("one", list.get(0));
        assertEquals(1, list.size());

        list.add(0, "two");
        assertEquals("two", list.get(0));
        assertEquals("one", list.get(1));
        assertEquals(2, list.size());

        list.add(2, "three");
        assertEquals("two", list.get(0));
        assertEquals("one", list.get(1));
        assertEquals("three", list.get(2));
        assertEquals(3, list.size());

        assertThrows(IndexOutOfBoundsException.class, () -> list.add(-1, "fail"));
        assertThrows(IndexOutOfBoundsException.class, () -> list.add(4, "fail"));
    }

    @Test
    void shouldUpdateElements() {
        var list = new ForwardLinkedList<String>();
        list.addAll(List.of("one", "two"));
        assertEquals("one", list.set(0, "one+"));
        assertEquals("one+", list.get(0));
        assertEquals("two", list.set(1, "two+"));
        assertEquals("two+", list.get(1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(-1, "fail"));
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(2, "fail"));
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(3, "fail"));
    }

    @Test
    void shouldRemoveElements() {
        var list = new ForwardLinkedList<>(List.of("one", "two", "three"));

        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(3));
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(4));

        assertEquals("one", list.remove(0));
        assertEquals(2, list.size());
        assertEquals("two", list.get(0));

        assertEquals("three", list.remove(1));
        assertEquals(1, list.size());
        assertEquals("two", list.get(0));

        assertTrue(list.remove("two"));
        assertTrue(list.isEmpty());
        assertEquals(0, list.size());
    }

    @Test
    void shouldModifyListInIterator() {
        var list = new ForwardLinkedList<>(List.of("one", "two", "three"));
        var iterator = list.listIterator();

        assertThrows(IllegalStateException.class, iterator::remove);
        assertThrows(IllegalStateException.class, () -> iterator.add("zero"));
        assertThrows(IllegalStateException.class, () -> iterator.set("zero+"));
        assertThrows(IllegalStateException.class, iterator::remove);

        assertEquals("one", iterator.next());
        iterator.add("zero");
        assertThrows(IllegalStateException.class, () -> iterator.add("zero"));
        assertThrows(IllegalStateException.class, () -> iterator.set("zero+"));
        assertThrows(IllegalStateException.class, iterator::remove);
        assertEquals(4, list.size());
        assertEquals("zero", list.get(0));

        assertEquals("one", iterator.next());
        iterator.remove();
        assertThrows(IllegalStateException.class, () -> iterator.add("one"));
        assertThrows(IllegalStateException.class, () -> iterator.set("one+"));
        assertThrows(IllegalStateException.class, iterator::remove);
        assertEquals(3, list.size());
        assertEquals("two", list.get(1));
        assertEquals("zero", iterator.previous());

        iterator.next();
        assertEquals("three", iterator.next());
        assertFalse(iterator.hasNext());
        iterator.remove();
        assertEquals(2, list.size());
        assertEquals("two", list.get(list.size() - 1));
        assertEquals("two", iterator.previous());
        iterator.set("two+");
        assertEquals("two+", list.get(1));
    }

    @Test
    void shouldIterateOverList() {
        String[] elements = { "one", "two", "three" };

        var list = new ForwardLinkedList<String>();
        Collections.addAll(list, elements);

        int index = 0;
        for (String element : list) {
            assertEquals(elements[index++], element);
        }
        assertEquals(3, index);
    }


    @Test
    void shouldNavigateUsingListIterator() {
        var list = new ForwardLinkedList<>(List.of("two", "one"));

        var iterator = list.listIterator();

        assertTrue(iterator.hasNext());
        assertFalse(iterator.hasPrevious());
        assertEquals(0, iterator.nextIndex());
        assertEquals(-1, iterator.previousIndex());
        assertThrows(NoSuchElementException.class, iterator::previous);

        assertEquals("two", iterator.next());
        assertTrue(iterator.hasNext());
        assertFalse(iterator.hasPrevious());
        assertEquals(-1, iterator.previousIndex());
        assertEquals(1, iterator.nextIndex());
        assertThrows(NoSuchElementException.class, iterator::previous);

        assertEquals("one", iterator.next());
        assertFalse(iterator.hasNext());
        assertEquals(2, iterator.nextIndex());
        assertThrows(NoSuchElementException.class, iterator::next);

        assertEquals("two", iterator.previous());
        assertTrue(iterator.hasNext());
        assertFalse(iterator.hasPrevious());
        assertEquals(-1, iterator.previousIndex());
        assertEquals(1, iterator.nextIndex());
    }
}