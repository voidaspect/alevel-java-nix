package com.alevel.java.nix.homework.lesson3;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class IntMinMaxTest {

    @Test
    void whenArrayEmpty_shouldReturnEmpty() {
        assertTrue(IntMinMax.of().isEmpty());
        assertTrue(IntMinMax.of(new int[0]).isEmpty());
    }

    @Test
    void shouldFindMinAndMax() {
        assertMinMax(0, 0, 0);
        assertMinMax(1, 1, 1, 1, 1);
        assertMinMax(1, 2, 2, 1, 1);
        assertMinMax(2, 3, 2, 3, 3);
        assertMinMax(-1, 1, 0, 1, -1);
        assertMinMax(-10, 10, 0, 4, 3, -10, 5, -3, 10, 9, 8, 4);
    }

    private static void assertMinMax(int min, int max, int... values) {
        Optional<IntMinMax> result = IntMinMax.of(values);
        assertTrue(result.isPresent());
        IntMinMax intMinMax = result.get();
        assertEquals(min, intMinMax.getMin());
        assertEquals(max, intMinMax.getMax());
    }
}