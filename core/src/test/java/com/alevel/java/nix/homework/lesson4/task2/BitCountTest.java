package com.alevel.java.nix.homework.lesson4.task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BitCountTest {


    @Test
    void shouldCountBits() {
        test(0, 0);
        test(63, Long.MAX_VALUE);
        test(64, -1);
        test(1, Long.MIN_VALUE);
        test(1, 1);
        test(1, 2);
        test(2, 3);
        test(1, 4);
        test(8, 255);
        test(5, 31);
        test(1, 32);
    }

    private static void test(int expected, long value) {
        var bitCount = BitCount.of(value);
        assertEquals(expected, bitCount.getCount());
    }
}