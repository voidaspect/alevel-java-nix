package com.alevel.java.nix.assignment.lesson26;

import com.alevel.java.nix.homework.lesson19.CsvTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvAnnotationMapperTest {
    private CsvAnnotationMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new CsvAnnotationMapper();
    }

    @Test
    void shouldMapTableToObjectList() throws Exception {
        Path source = Paths.get(getClass().getResource("example1.csv").toURI());
        CsvTable table = CsvTable.fromFile(source).orElseThrow();
        List<PersonalData> personalData = mapper.map(table, PersonalData.class);

        assertEquals(2, personalData.size());

        PersonalData person1 = personalData.get(0);
        assertEquals("Mike", person1.getName());
        assertEquals(27, person1.getAge());
        assertEquals(PersonalData.Gender.MALE, person1.getGender());
        assertEquals("janitor", person1.getOccupation());

        PersonalData person2 = personalData.get(1);
        assertEquals("Beth", person2.getName());
        assertEquals(23, person2.getAge());
        assertEquals(PersonalData.Gender.FEMALE, person2.getGender());
        assertEquals("recruiter", person2.getOccupation());
    }
}