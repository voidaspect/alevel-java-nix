package com.alevel.java.nix.homework.lesson5.task2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareIntMatrixTransposerTest {

    private SquareIntMatrixTransposer transposer;

    @BeforeEach
    void setUp() {
        transposer = new SquareIntMatrixTransposer();
    }

    @Test
    void ifMatrixIsEmpty_shouldNotCreateNewObject() {
        int[][] matrix = new int[0][];
        int[][] transposed = transposer.transpose(matrix);
        assertSame(matrix, transposed);
        assertArrayEquals(matrix, transposed);
    }

    @Test
    void shouldTransposeSquareMatrix() {
        test(new int[][]{{0}}, new int[][]{{0}});
        test(new int[][]{
                {0, 2},
                {1, 3}
        }, new int[][]{
                {0, 1},
                {2, 3}
        });
        test(new int[][]{
                {0, 3, 6},
                {1, 4, 7},
                {2, 5, 8}
        }, new int[][]{
                {0, 1, 2},
                {3, 4, 5},
                {6, 7, 8}
        });
        test(new int[][]{
                {0, 4, 8, 12},
                {1, 5, 9, 13},
                {2, 6, 10, 14},
                {3, 7, 11, 15}
        }, new int[][]{
                {0, 1, 2, 3},
                {4, 5, 6, 7},
                {8, 9, 10, 11},
                {12, 13, 14, 15}
        });
    }

    @Test
    void whenMatrixIsNotSquare_shouldThrowOutOfBoundsException() {
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> transposer.transpose(new int[][] {
                {0, 1},
                {2, 3},
                {4, 5}
        }));
    }

    private void test(int[][] expected, int[][] matrix) {
        int[][] transposed = transposer.transpose(matrix);
        assertSame(transposed, matrix); // should transpose matrix in-place
        assertArrayEquals(expected, transposed);
    }
}