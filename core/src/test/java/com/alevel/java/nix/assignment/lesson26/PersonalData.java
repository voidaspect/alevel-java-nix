package com.alevel.java.nix.assignment.lesson26;

public class PersonalData {

    @CsvColumn("name")
    private String name;

    @CsvColumn("age")
    private int age;

    @CsvColumn("gender")
    private Gender gender;

    @CsvColumn("occupation")
    private String occupation;

    enum Gender {MALE, FEMALE}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
}
