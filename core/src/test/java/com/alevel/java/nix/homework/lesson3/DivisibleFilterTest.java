package com.alevel.java.nix.homework.lesson3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivisibleFilterTest {

    @Test
    void whenDivisorIs0_shouldFailToConfigure() {
        assertThrows(IllegalArgumentException.class, () -> new DivisibleFilter(0));
    }

    @Test
    void shouldFilter() {
        test(1, new int[]{});
        test(1, new int[]{0, 1, 2, 3}, 0, 1, 2, 3);
        test(2, new int[]{2}, 1, 2, 3);
        test(2, new int[]{2, 4, 6}, 1, 2, 3, 1, 1, 4, -9, 6);
        test(3, new int[]{3, -3, 0}, 3, 10, 8, -3, 0);
        test(-3, new int[]{3, -3, 0}, 3, 10, 8, -3, 0);
    }

    @Test
    void shouldReturnItselfOnEmptyArray() {
        int[] empty = new int[]{};
        assertSame(empty, filter(1, empty));
        assertSame(empty, filter(2, empty));
        assertSame(empty, filter(-2, empty));
    }

    private static void test(int divisor, int[] expected, int... values) {
        int[] filtered = filter(divisor, values);
        assertArrayEquals(expected, filtered);
    }

    private static int[] filter(int divisor, int... values) {
        var filter = new DivisibleFilter(divisor);
        return filter.filter(values);
    }

}