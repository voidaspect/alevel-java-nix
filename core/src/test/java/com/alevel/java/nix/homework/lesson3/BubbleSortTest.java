package com.alevel.java.nix.homework.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class BubbleSortTest {

    private BubbleSort bubbleSort;

    @BeforeEach
    void setUp() {
        bubbleSort = new BubbleSort();
    }

    @Test
    void shouldSort() {
        test(new int[]{});
        test(new int[]{0}, 0);
        test(new int[]{0, 1}, 0, 1);
        test(new int[]{-1, 0}, 0, -1);
        int[] oneToTen = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        test(oneToTen, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        test(oneToTen, 1, 3, 5, 10, 2, 4, 6, 8, 9, 7);
        test(oneToTen, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
    }

    private void test(int[] expected, int... values) {
        bubbleSort.sort(values);
        assertArrayEquals(expected, values);
    }
}