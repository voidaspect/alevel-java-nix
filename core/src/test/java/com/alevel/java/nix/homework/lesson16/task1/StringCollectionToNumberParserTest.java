package com.alevel.java.nix.homework.lesson16.task1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StringCollectionToNumberParserTest {

    private StringCollectionToNumberParser subject;

    @BeforeEach
    void setUp() {
        subject = new StringCollectionToNumberParser();
    }

    @Test
    void shouldParseNumbersFromStrings() {
        assertEquals(12345L, subject.parse(List.of("string 1 text", "2 string 3 text", "45")));
        assertEquals(45L, subject.parse(List.of("string 0 text", "0 string 0 text", "45")));
        assertEquals(0L, subject.parse(List.of("string 0 text", "0 string 0 text", "00")));
        assertEquals(10000L, subject.parse(List.of("string 1 text", "0 string 0 text", "00")));
    }

    @Test
    void whenStringsContainNoDigits_ShouldThrowIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> subject.parse(List.of()));
        assertThrows(IllegalArgumentException.class, () -> subject.parse(List.of("s")));
        assertThrows(IllegalArgumentException.class, () -> subject.parse(List.of("s-one", "s-two")));
    }
}