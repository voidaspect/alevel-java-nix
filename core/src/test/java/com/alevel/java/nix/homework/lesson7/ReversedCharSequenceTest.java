package com.alevel.java.nix.homework.lesson7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReversedCharSequenceTest {

    @Test
    void shouldReverseString() {
        test("", "");
        test("a", "a");
        test("ba", "ab");
        test("pop", "pop");
        test("big data", "atad gib");
        test("Юнікод", "докінЮ");
    }

    @Test
    void shouldPropagateNull() {
        assertNull(ReversedCharSequence.of(null));
    }

    @Test
    void shouldGetCharacterByIndex() {
        var reversed = ReversedCharSequence.of("abc");
        assertEquals('c', reversed.charAt(0));
        assertEquals('b', reversed.charAt(1));
        assertEquals('a', reversed.charAt(2));
    }

    @Test
    void shouldReturnSubSequence() {
        var reversed = ReversedCharSequence.of("input");
        assertContentEquals("tupni", reversed);
        assertContentEquals("tup", reversed.subSequence(0, 3));
        assertContentEquals("t", reversed.subSequence(0, 1));
        assertContentEquals("", reversed.subSequence(0, 0));
        assertContentEquals("tupni", reversed.subSequence(0, 5));
        assertEquals(reversed, reversed.subSequence(0, 5));
        assertSame(reversed, reversed.subSequence(0, 5));
    }

    @Test
    void shouldCompareToOtherCharSequence() {
        var reversed = ReversedCharSequence.of("cba");
        assertEquals(0, CharSequence.compare(reversed, "abc"));
        assertTrue(CharSequence.compare(reversed, "abd") < 0);
        assertTrue(CharSequence.compare(reversed, "aa") > 0);
    }

    @Test
    void shouldCheckRange() {
        var reversed = ReversedCharSequence.of("input");
        testOutOfBounds(reversed, -1, 2);
        testOutOfBounds(reversed, 3, 2);
        testOutOfBounds(reversed, -1);
        testOutOfBounds(reversed, 0, 6);
        testOutOfBounds(reversed, 5);
    }

    private static void testOutOfBounds(CharSequence seq, int index) {
        var exception = assertThrows(StringIndexOutOfBoundsException.class, () -> seq.charAt(index));
        assertEquals("index " + index + ", length " + seq.length(), exception.getMessage());
    }

    private static void testOutOfBounds(CharSequence seq, int start, int end) {
        int length = seq.length();
        var exception = assertThrows(StringIndexOutOfBoundsException.class, () -> seq.subSequence(start, end));
        assertEquals("start " + start + ", end " + end + ", length " + length, exception.getMessage());
    }

    private static void test(String expected, CharSequence input) {
        var reversed = ReversedCharSequence.of(input);

        assertEquals(input.length(), reversed.length());

        var reversedTwice = ReversedCharSequence.of(reversed);
        assertSame(input, reversedTwice);

        assertContentEquals(expected, reversed);
        assertEquals(expected, reversed.toString());
    }

    private static void assertContentEquals(String expected, CharSequence input) {
        assertTrue(
                expected.contentEquals(input),
                () -> "Expected \"" + expected + "\" but got \"" + input + '"'
        );
    }
}