package com.alevel.java.nix.homework.lesson4.task1;

import com.alevel.java.nix.homework.lesson4.print.Printable;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;

import static org.junit.jupiter.api.Assertions.*;

class DigitFizzBuzzTest {

    private static final String NL = System.lineSeparator();

    @Test
    void shouldPrintFizzBuzzRightToLeft() {
        testRightToLeft(
                347_693_485,
                "5" + NL + "fizz" + NL + "fizz" + NL
                        + "buzz" + NL + "buzz" + NL + "fizzbuzz" + NL
                        + "7" + NL + "fizz" + NL + "buzz" + NL
        );
        testRightToLeft(0, "fizzbuzz" + NL);
        testRightToLeft(1, "1" + NL);
        testRightToLeft(3, "buzz" + NL);
        testRightToLeft(4, "fizz" + NL);
        testRightToLeft(6, "fizzbuzz" + NL);
        testRightToLeft(-6, "fizzbuzz" + NL);
        testRightToLeft(20, "fizzbuzz" + NL + "fizz" + NL);
        testRightToLeft(200, "fizzbuzz" + NL + "fizzbuzz" + NL + "fizz" + NL);
        testRightToLeft(1111111111, ("1" + NL).repeat(10));
    }

    @Test
    void shouldPrintFizzBuzzLeftToRight() {
        testLeftToRight(
                347_693_485,
                "buzz" + NL + "fizz" + NL + "7" + NL +
                        "fizzbuzz" + NL + "buzz" + NL + "buzz" + NL +
                        "fizz" + NL + "fizz" + NL + "5" + NL
        );
        testLeftToRight(0, "fizzbuzz" + NL);
        testLeftToRight(1, "1" + NL);
        testLeftToRight(3, "buzz" + NL);
        testLeftToRight(4, "fizz" + NL);
        testLeftToRight(6, "fizzbuzz" + NL);
        testLeftToRight(-6, "fizzbuzz" + NL);
        testLeftToRight(20, "fizz" + NL + "fizzbuzz" + NL);
        testLeftToRight(100, "1" + NL + "fizzbuzz" + NL + "fizzbuzz" + NL);
        testLeftToRight(1111111111, ("1" + NL).repeat(10));
    }

    private static void testRightToLeft(int sourceValue, String expected) {
        assertFizzBuzz(sourceValue, expected, DigitFizzBuzz.rightToLeft(sourceValue));
    }

    private static void testLeftToRight(int sourceValue, String expected) {
        assertFizzBuzz(sourceValue, expected, DigitFizzBuzz.leftToRight(sourceValue));
    }

    private static void assertFizzBuzz(int sourceValue, String expected, DigitFizzBuzz fizzBuzz) {
        assertEquals(sourceValue, fizzBuzz.getSource());
        assertEquals(expected, fizzBuzz.getText());
        assertEquals(expected, interceptOutput(fizzBuzz));
    }

    private static String interceptOutput(Printable result) {
        var byteArrayOutput = new ByteArrayOutputStream();
        try (byteArrayOutput) {
            result.print(byteArrayOutput);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return byteArrayOutput.toString();
    }
}