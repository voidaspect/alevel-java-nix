package com.alevel.java.nix.homework.lesson9.model;

import com.alevel.java.nix.homework.lesson9.view.TurnResultView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class TicTacToe3x3 implements TicTacToe {

    private static final Logger log = LoggerFactory.getLogger(TicTacToe3x3.class);

    private static final int SIDE = 3;

    private final Player[][] board = new Player[SIDE][SIDE];

    private int marks = 0;

    private Player currentPlayer = Player.X;

    private TurnResult previousTurnResult = TurnResult.CONTINUE;

    private TurnResultView cachedView = createView();

    @Override
    public void reset() {
        for (Player[] row : board) {
            Arrays.fill(row, null);
        }
        marks = 0;
        previousTurnResult = TurnResult.CONTINUE;
        cachedView = createView();
        currentPlayer = Player.X;
        log.info("Game state reset");
    }

    @Override
    public TurnResultView mark(int row, int col) {
        log.info("{} turn", currentPlayer);
        if (previousTurnResult != TurnResult.CONTINUE) {
            log.warn("Game is finished, no new turns are possible");
            return cachedView;
        }
        if (invalid(row)) {
            log.warn("Row index {} outside the board, repeat turn for player {}", row, currentPlayer);
            return cachedView;
        }
        if (invalid(col)) {
            log.warn("Col index {} outside the board, repeat turn for player {}", col, currentPlayer);
            return cachedView;
        }
        Player mark = board[row][col];
        if (mark == null) {
            board[row][col] = currentPlayer;
            log.info("Player {} marks row {} col {}", currentPlayer, row, col);
            marks++;
        } else {
            log.warn("Row {}, Col {} already marked by {}, repeat turn for player {}", row, col, mark, currentPlayer);
            return cachedView;
        }
        previousTurnResult = getTurnResult(row, col);
        currentPlayer = currentPlayer == Player.X ? Player.O : Player.X;
        cachedView = createView();
        return cachedView;
    }

    private TurnResult getTurnResult(int row, int col) {
        boolean victory = winByRow(row) || winByCol(col)
                || (row == col && winByDiagonal())
                || (row == SIDE - col - 1 && winByReverseDiagonal());
        if (victory) {
            log.info("Player {} wins!", currentPlayer);
            return currentPlayer.victory();
        }
        if (boardFull()) {
            log.info("Game is a tie!");
            return TurnResult.TIE;
        }
        return TurnResult.CONTINUE;
    }

    @Override
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    @Override
    public TurnResultView view() {
        return cachedView;
    }

    private TurnResultView createView() {
        var nl = System.lineSeparator();
        var view = new StringBuilder();
        view.append("# 0 1 2").append(nl);
        for (int row = 0; row < SIDE; row++) {
            view.append(row);
            for (int col = 0; col < SIDE; col++) {
                var player = board[row][col];
                char mark = player == null ? '•' : player.mark();
                view.append(' ').append(mark);
            }
            view.append(nl);
        }
        return new TurnResultView(previousTurnResult, view.toString());
    }

    private boolean boardFull() {
        return marks == 9;
    }

    private boolean winByRow(int row) {
        for (int col = 0; col < SIDE; col++) {
            if (currentPlayer != board[row][col]) return false;
        }
        return true;
    }

    private boolean winByCol(int col) {
        for (int row = 0; row < SIDE; row++) {
            if (currentPlayer != board[row][col]) return false;
        }
        return true;
    }

    private boolean winByDiagonal() {
        for (int i = 0; i < SIDE; i++) {
            if (currentPlayer != board[i][i]) return false;
        }
        return true;
    }

    private boolean winByReverseDiagonal() {
        for (int i = 0; i < SIDE; i++) {
            if (currentPlayer != board[i][SIDE - i - 1]) return false;
        }
        return true;
    }

    private static boolean invalid(int index) {
        return index < 0 || index >= SIDE;
    }


}
