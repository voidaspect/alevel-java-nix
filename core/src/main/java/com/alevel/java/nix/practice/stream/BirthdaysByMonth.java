package com.alevel.java.nix.practice.stream;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public final class BirthdaysByMonth {

    private final Map<Month, Integer> count;

    private BirthdaysByMonth(Map<Month, Integer> count) {
        this.count = count;
    }

    public static BirthdaysByMonth of(Collection<LocalDate> birthdays) {
        var count = birthdays.stream().collect(groupingBy(
                LocalDate::getMonth,
                () -> new EnumMap<>(Month.class),
                summingInt(date -> 1)));

        return new BirthdaysByMonth(count);
    }

    public int get(Month month) {
        return count.getOrDefault(month, 0);
    }

}
