package com.alevel.java.nix.homework.lesson9;

import com.alevel.java.nix.homework.lesson9.controller.TicTacToeGameLoop;
import com.alevel.java.nix.homework.lesson9.model.TicTacToe3x3;

public class TicTacToeRunner {
    public static void main(String[] args) {
        var gameLoop = new TicTacToeGameLoop(System.in, System.out, new TicTacToe3x3());
        gameLoop.loop();
    }
}
