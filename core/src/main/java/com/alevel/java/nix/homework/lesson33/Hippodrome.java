package com.alevel.java.nix.homework.lesson33;

import java.util.Scanner;
import java.util.StringJoiner;

public class Hippodrome {

    private static final int RACE_LENGTH = 1000;

    public static void main(String[] args) {

        Race race = new Race(RACE_LENGTH);

        Race.Horse[] horses = {
                new Race.Horse("Horse One"),
                new Race.Horse("Horse Two"),
                new Race.Horse("Horse Three"),
                new Race.Horse("Horse Four")
        };

        for (Race.Horse horse : horses) {
            horse.addToRace(race);
        }

        int numberOfHorses = horses.length;

        String nl = System.lineSeparator();

        StringJoiner prompt = new StringJoiner(nl, "Bet on a horse:" + nl, nl);
        for (int i = 0; i < numberOfHorses; i++) {
            prompt.add(i + 1 + ". " + horses[i].getName());
        }
        System.out.print(prompt);

        int horseNumber;
        Scanner scanner = new Scanner(System.in);
        while ((horseNumber = scanner.nextInt()) > numberOfHorses || horseNumber <= 0) {
            System.out.println("No horse with such number is present. Choose once again!");
        }

        Race.Horse chosen = horses[horseNumber - 1];
        System.out.printf("You've chosen %s%n%n", chosen.getName());

        race.startAndWait();

        int place = race.getPlace(chosen);
        System.out.printf("%nHorse you had bet on got place %d out of %d%n", place, numberOfHorses);

    }
}
