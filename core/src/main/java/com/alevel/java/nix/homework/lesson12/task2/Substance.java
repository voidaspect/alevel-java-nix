package com.alevel.java.nix.homework.lesson12.task2;

public interface Substance {

    double ABSOLUTE_ZERO_CELSIUS = -273.15;

    double ROOM_TEMPERATURE = 20.0;

    void heatUp(double t);

    double getTemperature();

    State getState();

}
