package com.alevel.java.nix.homework.lesson25;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public final class PropertyHolderFactory {

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> T create(Properties properties, Class<T> c) {

        try {
            Constructor<T> constructor = c.getConstructor();

            T target = constructor.newInstance();

            for (Field field : c.getFields()) {
                PropertyKey key = field.getAnnotation(PropertyKey.class);
                if (key == null) continue;
                String prop = properties.getProperty(key.value());
                if (prop == null) continue;

                Class<?> type = field.getType();
                if (type == String.class) {
                    field.set(target, prop);
                } else if (type.isEnum()) {
                    field.set(target, Enum.valueOf((Class<Enum>) type, prop));
                } else if (type == int.class || type == Integer.class) {
                    field.setInt(target, Integer.parseInt(prop));
                } else if (type == long.class || type == Long.class) {
                    field.setLong(target, Long.parseLong(prop));
                } else if (type == double.class || type == Double.class) {
                    field.setDouble(target, Double.parseDouble(prop));
                } else if (type == boolean.class || type == Boolean.class) {
                    field.setBoolean(target, Boolean.parseBoolean(prop));
                } else {
                    throw new UnsupportedOperationException("Unsupported field type (" +
                            type.getName() + ") is required for field " +
                            field.getName());
                }
            }

            return target;
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

    }

}
