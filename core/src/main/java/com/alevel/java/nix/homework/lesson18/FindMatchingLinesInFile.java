package com.alevel.java.nix.homework.lesson18;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FindMatchingLinesInFile {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Too few arguments");
            return;
        }

        Path path = Paths.get(args[0]);
        Pattern pattern = Pattern.compile(args[1]);

        String output = new FindMatchingLinesInFile().find(path, pattern)
                .collect(Collectors.joining(System.lineSeparator()));

        System.out.println(output);
    }

    public Stream<String> find(Path path, Pattern pattern) {
        Stream<String> lines;
        try {
            lines = Files.lines(path);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return lines.filter(line -> pattern.matcher(line).find());
    }

}
