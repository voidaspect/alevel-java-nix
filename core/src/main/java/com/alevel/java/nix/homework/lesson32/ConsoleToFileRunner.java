package com.alevel.java.nix.homework.lesson32;

import java.nio.file.Paths;

public class ConsoleToFileRunner {

    public static final String DEFAULT_OUTPUT_PATH = "output.txt";

    public static void main(String[] args) {

        var output = args.length > 0 ? args[0] : DEFAULT_OUTPUT_PATH;

        var input = new ConcurrentBox<String>();

        var producer = new Thread(
                new MessageProducer(input),
                "Msg Producer"
        );
        var consumer = new Thread(
                new MessageConsumer(input, Paths.get(output)),
                "Msg Consumer"
        );

        producer.start();
        consumer.start();
    }
}
