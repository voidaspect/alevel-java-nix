package com.alevel.java.nix.homework.lesson6.task3;

public final class MaxArea {

    public int maxArea(int[] heights) {
        int length = heights.length;
        if (length < 2) {
            return 0;
        }
        int maxArea = 0;
        for (int start = 0, end = length - 1, width = end - start; width > 0; width--) {
            int startHeight = heights[start];
            int endHeight = heights[end];
            int height;
            if (startHeight < endHeight) {
                start++;
                height = startHeight;
            } else {
                end--;
                height = endHeight;
            }
            int area = width * height;
            maxArea = Math.max(maxArea, area);
        }
        return maxArea;
    }

}
