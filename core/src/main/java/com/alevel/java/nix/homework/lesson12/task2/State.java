package com.alevel.java.nix.homework.lesson12.task2;

public enum State {
    SOLID, LIQUID, GAS
}
