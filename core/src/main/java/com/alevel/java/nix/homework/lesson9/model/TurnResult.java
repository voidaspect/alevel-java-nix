package com.alevel.java.nix.homework.lesson9.model;

public enum TurnResult {
    CONTINUE("continue"),
    X_WON("x won"),
    O_WON("o won"),
    TIE("tie");

    private final String displayName;

    TurnResult(String displayName) {
        this.displayName = displayName;
    }

    public String displayName() {
        return displayName;
    }
}
