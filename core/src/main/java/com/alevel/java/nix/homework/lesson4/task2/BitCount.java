package com.alevel.java.nix.homework.lesson4.task2;

public final class BitCount {

    private final int count;

    private BitCount(int count) {
        this.count = count;
    }

    public static BitCount of(long value) {
        int count = Long.bitCount(value);
        return new BitCount(count);
    }

    //region boilerplate

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BitCount)) return false;

        BitCount bitCount = (BitCount) o;

        return count == bitCount.count;
    }

    @Override
    public int hashCode() {
        return count;
    }

    //endregion
}
