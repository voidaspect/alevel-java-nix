package com.alevel.java.nix.homework.lesson3;

import java.util.Optional;

public final class IntMinMax {

    private final int min;

    private final int max;

    private IntMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public static Optional<IntMinMax> of(int... values) {
        int length = values.length;
        if (length == 0) {
            return Optional.empty();
        }

        int min = values[0];
        int max = min;

        for (int i = 1; i < length; i++) {
            int element = values[i];
            if (element > max) {
                max = element;
            } else if (element < min) {
                min = element;
            }
        }

        return Optional.of(new IntMinMax(min, max));
    }

    //region boilerplate

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntMinMax)) return false;

        IntMinMax intMinMax = (IntMinMax) o;

        if (min != intMinMax.min) return false;
        return max == intMinMax.max;
    }

    @Override
    public int hashCode() {
        int result = min;
        result = 31 * result + max;
        return result;
    }

    @Override
    public String toString() {
        return "IntMinMax{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }

    //endregion
}
