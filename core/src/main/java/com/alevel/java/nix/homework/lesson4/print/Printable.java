package com.alevel.java.nix.homework.lesson4.print;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Writer;
import java.nio.channels.ByteChannel;
import java.nio.file.Path;

public interface Printable {

    void print();

    void print(OutputStream out) throws IOException;

    void print(Writer out) throws IOException;

    void print(PrintStream out);

    void print(Path path) throws IOException;

    void print(ByteChannel out) throws IOException;

}
