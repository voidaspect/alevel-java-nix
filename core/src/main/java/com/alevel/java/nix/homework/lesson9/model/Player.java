package com.alevel.java.nix.homework.lesson9.model;

public enum Player {

    X(TurnResult.X_WON, 'x'), O(TurnResult.O_WON, 'o');

    private final TurnResult victory;

    private final char mark;

    Player(TurnResult victory, char mark) {
        this.victory = victory;
        this.mark = mark;
    }

    public TurnResult victory() {
        return victory;
    }

    public char mark() {
        return mark;
    }
}
