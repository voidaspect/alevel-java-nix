package com.alevel.java.nix.homework.lesson5.task2;

public final class NonRectangleIntMatrixTransposer extends AbstractIntMatrixTransposer {

    @Override
    int[][] transpose(int[][] matrix, int rows) {
        int maxRowLength = 0;
        for (int[] row : matrix) {
            maxRowLength = Math.max(maxRowLength, row.length);
        }
        if (maxRowLength == 0) {
            return new int[0][];
        }
        int[][] transposedMatrix = new int[maxRowLength][rows];
        for (int i = 0; i < rows; i++) {
            for (int j = 0, rowLength = matrix[i].length; j < rowLength; j++) {
                transposedMatrix[j][i] = matrix[i][j];
            }
        }
        return transposedMatrix;

    }
}
