package com.alevel.java.nix.homework.lesson32;

import java.util.Scanner;

public class MessageProducer implements Runnable {

    public static final String TERMINAL_MESSAGE = "quit";

    private final ConcurrentBox<String> input;

    public MessageProducer(ConcurrentBox<String> input) {
        this.input = input;
    }

    @Override
    public void run() {
        var scanner = new Scanner(System.in);
        String input;
        while (!TERMINAL_MESSAGE.equals(input = scanner.nextLine())) {
            this.input.put(input);
        }
        this.input.dispose();
    }
}
