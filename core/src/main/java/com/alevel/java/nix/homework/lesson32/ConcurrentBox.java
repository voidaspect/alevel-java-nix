package com.alevel.java.nix.homework.lesson32;

import java.util.Optional;

public class ConcurrentBox<T> {

    private volatile T value;

    private volatile boolean disposed;

    public void put(T value) {
        if (disposed) return;
        this.value = value;
    }

    public Optional<T> get() {
        return Optional.ofNullable(value);
    }

    public boolean isDisposed() {
        return disposed;
    }

    public void dispose() {
        this.disposed = true;
    }
}
