package com.alevel.java.nix.homework.lesson9.controller;

import com.alevel.java.nix.homework.lesson9.model.TicTacToe;
import com.alevel.java.nix.homework.lesson9.model.TurnResult;
import com.alevel.java.nix.homework.lesson9.view.TurnResultView;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class TicTacToeGameLoop {

    private final InputStream input;

    private final PrintStream output;

    private final TicTacToe model;

    public TicTacToeGameLoop(InputStream input, PrintStream output, TicTacToe model) {
        this.input = input;
        this.output = output;
        this.model = model;
    }

    public void loop() {
        output.println("Game started");
        model.view().print(output);
        var scanner = new Scanner(input);

        TurnResult turn;
        do {
            output.println("Player " + model.getCurrentPlayer() + ", place your mark (enter row and column):");
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            TurnResultView resultView = model.mark(row, col);
            turn = resultView.result();
            resultView.print(output);
        } while (turn == TurnResult.CONTINUE);

        output.println("Game over, " + turn.displayName());
        model.reset();
    }
}
