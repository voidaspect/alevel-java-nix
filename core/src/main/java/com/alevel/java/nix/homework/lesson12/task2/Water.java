package com.alevel.java.nix.homework.lesson12.task2;

public final class Water extends AbstractSubstance {

    @Override
    protected double getMeltingPoint() {
        return 0.0;
    }

    @Override
    protected double getBoilingPoint() {
        return 100.0;
    }

}
