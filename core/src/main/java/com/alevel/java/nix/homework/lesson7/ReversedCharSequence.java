package com.alevel.java.nix.homework.lesson7;

public class ReversedCharSequence implements CharSequence {

    private final CharSequence source;

    private ReversedCharSequence(CharSequence source) {
        this.source = source;
    }

    public static CharSequence of(CharSequence source) {
        if (source == null) {
            return null;
        }
        if (source instanceof ReversedCharSequence) {
            return ((ReversedCharSequence) source).source;
        }
        if (source.length() < 2) {
            return source;
        }
        return new ReversedCharSequence(source);
    }

    @Override
    public int length() {
        return source.length();
    }

    @Override
    public char charAt(int index) {
        try {
            return source.charAt(source.length() - 1 - index);
        } catch (IndexOutOfBoundsException e) {
            throw new StringIndexOutOfBoundsException("index " + index + ", length " + source.length());
        }
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        int length = source.length();
        if (start == 0 && end == length) {
            return this;
        }
        CharSequence sourceSubSequence;
        try {
            sourceSubSequence = source.subSequence(length - end, length - start);
        } catch (IndexOutOfBoundsException e) {
            throw new StringIndexOutOfBoundsException("start " + start + ", end " + end + ", length " + length);
        }
        if (sourceSubSequence.length() < 2) {
            return sourceSubSequence;
        }
        return new ReversedCharSequence(sourceSubSequence);
    }

    @Override
    public String toString() {
        int length = source.length();
        return new StringBuilder(length).append(this, 0, length).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReversedCharSequence)) return false;

        ReversedCharSequence that = (ReversedCharSequence) o;

        return source.equals(that.source);
    }

    @Override
    public int hashCode() {
        return ~source.hashCode();
    }
}
