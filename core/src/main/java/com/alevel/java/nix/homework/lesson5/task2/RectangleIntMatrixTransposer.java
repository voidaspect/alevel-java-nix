package com.alevel.java.nix.homework.lesson5.task2;

public final class RectangleIntMatrixTransposer extends AbstractIntMatrixTransposer {

    @Override
    int[][] transpose(int[][] matrix, int rows) {
        int rowLength = matrix[0].length;
        if (rowLength == 0) {
            return new int[0][];
        }
        int[][] transposed = new int[rowLength][rows];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rowLength; j++) {
                transposed[j][i] = matrix[i][j];
            }
        }
        return transposed;
    }

}
