package com.alevel.java.nix.homework.lesson16.task2;

import java.util.*;

public class ForwardLinkedList<E> extends AbstractSequentialList<E> {

    private int size = 0;

    private Node<E> head;

    public ForwardLinkedList() {
    }

    public ForwardLinkedList(Collection<E> elements) {
        if (elements.isEmpty()) return;
        size = 1;
        var iterator = elements.iterator();
        head = new Node<>(iterator.next());
        var current = head;
        while (iterator.hasNext()) {
            var next = new Node<>(iterator.next());
            current.next = next;
            current = next;
            size++;
        }
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return new ListItr();
    }

    @Override
    public E get(int index) {
        Objects.checkIndex(index, size);
        return node(index).value;
    }

    @Override
    public E set(int index, E element) {
        Objects.checkIndex(index, size);
        var node = node(index);
        var value = node.value;
        node.value = element;
        return value;
    }

    @Override
    public void add(int index, E element) {
        Objects.checkIndex(index, size + 1);
        if (index == 0) {
            head = new Node<>(element, head);
        } else {
            var previous = node(index - 1);
            previous.next = new Node<>(element, previous.next);
        }
        size++;
    }

    @Override
    public boolean add(E e) {
        var node = new Node<>(e);
        if (size == 0) {
            this.head = node;
        } else {
            node(size - 1).next = node;
        }
        size++;
        return true;
    }

    @Override
    public E remove(int index) {
        Objects.checkIndex(index, size);
        final E value;
        if (index == 0) {
            value = head.value;
            head = head.next;
        } else {
            var before = node(index - 1);
            var removed = before.next;
            before.next = removed.next;
            value = removed.value;
        }
        size--;
        return value;
    }

    @Override
    public boolean remove(Object o) {
        if (isEmpty()) return false;
        if (Objects.equals(head.value, o)) {
            head = head.next;
            size--;
            return true;
        }
        var previous = head;
        var next = head.next;
        while (next != null) {
            if (Objects.equals(next, o)) {
                previous.next = next.next;
                size--;
                return true;
            }
            previous = next;
            next = next.next;
        }
        return false;
    }

    private Node<E> node(int index) {
        var current = head;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        return current;
    }

    @Override
    public int size() {
        return size;
    }

    private static final class Node<E> {

        E value;

        Node<E> next;

        public Node(E value) {
            this.value = value;
        }

        public Node(E value, Node<E> next) {
            this.value = value;
            this.next = next;
        }
    }

    private final class ListItr implements ListIterator<E> {

        int index = -1;

        boolean moved;

        Node<E> current;

        Node<E> previous;

        Node<E> next = head;

        @Override
        public boolean hasNext() {
            return index + 1 < size;
        }

        @Override
        public E next() {
            if (!hasNext()) throw new NoSuchElementException("No element at index " + index + 1);
            index++;
            previous = current;
            current = next;
            next = next.next;
            moved = false;
            return current.value;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public E previous() {
            if (!hasPrevious()) throw new NoSuchElementException("No element at index " + (index - 1));
            current = previous;
            next = current.next;
            if (--index == 0) {
                previous = null;
            } else {
                previous = node(index);
            }
            moved = false;
            return current.value;
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return Math.max(index - 1, -1);
        }

        @Override
        public void remove() {
            if (index < 0) {
                throw new IllegalStateException("Can't remove element, next was never called");
            }
            if (moved) {
                throw new IllegalStateException("Can't remove element, add or remove already called");
            }
            if (index == 0) {
                head = head.next;
            } else {
                previous.next = next;
            }
            size--;
            moved = true;
        }

        @Override
        public void set(E e) {
            if (index < 0) {
                throw new IllegalStateException("Can't modify element, next was never called");
            }
            if (moved) {
                throw new IllegalStateException("Can't modify element, add or remove already called");
            }
            current.value = e;
        }

        @Override
        public void add(E e) {
            if (index < 0) {
                throw new IllegalStateException("Can't add element, next was never called");
            }
            if (moved) {
                throw new IllegalStateException("Can't add element, add or remove already called");
            }
            this.next = current;
            current = new Node<>(e, this.current);
            if (index == 0) {
                head = current;
            } else {
                previous.next = current;
            }
            size++;
            moved = true;
        }
    }
}
