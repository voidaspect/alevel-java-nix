package com.alevel.java.nix.homework.lesson25;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class Runner {

    public static void main(String[] args) {

        Properties props;

        try(Reader input = Files.newBufferedReader(Paths.get("io/config/app.properties"))) {
            props = new Properties();
            props.load(input);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        PropertyHolderFactory factory = new PropertyHolderFactory();

        AppProperties appProperties = factory.create(props, AppProperties.class);

        System.out.println("client url: " + appProperties.url);
        System.out.println("client timeout: " + appProperties.timeout);
        System.out.println("client retry: " + appProperties.retry);
    }

}

