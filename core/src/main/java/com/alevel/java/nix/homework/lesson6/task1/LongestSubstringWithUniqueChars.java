package com.alevel.java.nix.homework.lesson6.task1;

import java.util.HashMap;
import java.util.Map;

public final class LongestSubstringWithUniqueChars {

    public int lengthOfLongestSubstring(String s) {
        int length = s.length();
        if (length < 2) {
            return length;
        }
        Map<Character, Integer> charToLastPosition = new HashMap<>();
        charToLastPosition.put(s.charAt(0), 0);
        int maxSubStringLength = 1;
        for (int i = 1, substringStartIndex = -1; i < length; i++) {
            Integer previousPosition = charToLastPosition.put(s.charAt(i), i);
            if (previousPosition != null) {
                substringStartIndex = Math.max(substringStartIndex, previousPosition);
            }
            maxSubStringLength = Math.max(maxSubStringLength, i - substringStartIndex);
        }
        return maxSubStringLength;
    }

}
