package com.alevel.java.nix.homework.lesson4.print;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class PrintableTextResult implements Printable {

    public abstract String getText();

    @Override
    public void print() {
        print(System.out);
    }

    @Override
    public void print(OutputStream out) throws IOException {
        out.write(getTextBytes());
    }

    @Override
    public void print(Writer out) throws IOException {
        out.write(getText());
    }

    @Override
    public void print(PrintStream out) {
        out.print(getText());
    }

    @Override
    public void print(Path path) throws IOException {
        Files.writeString(path, getText());
    }

    @Override
    public void print(ByteChannel out) throws IOException {
        out.write(ByteBuffer.wrap(getTextBytes()));
    }

    private byte[] getTextBytes() {
        return getText().getBytes();
    }

}
