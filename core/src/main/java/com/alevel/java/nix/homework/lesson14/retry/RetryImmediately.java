package com.alevel.java.nix.homework.lesson14.retry;

import com.alevel.java.nix.homework.lesson14.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class RetryImmediately<T> extends Retry<T> {

    private static final Logger log = LoggerFactory.getLogger(RetryImmediately.class);

    RetryImmediately(int times, Block<T> block) {
        super(times, block);
    }

    @Override
    void backoff(int attempt) {
        log.warn("Retrying immediately");
    }
}
