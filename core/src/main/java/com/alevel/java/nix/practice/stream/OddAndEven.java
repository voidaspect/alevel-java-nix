package com.alevel.java.nix.practice.stream;

import java.util.*;
import java.util.function.Supplier;

import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toCollection;

public final class OddAndEven {

    private final SortedSet<Integer> evenNumbers;

    private final SortedSet<Integer> oddNumbers;

    private OddAndEven(SortedSet<Integer> evenNumbers, SortedSet<Integer> oddNumbers) {
        this.evenNumbers = Collections.unmodifiableSortedSet(evenNumbers);
        this.oddNumbers = Collections.unmodifiableSortedSet(oddNumbers);
    }

    public static OddAndEven of(int... numbers) {
        Supplier<TreeSet<Integer>> collectionFactory = () -> new TreeSet<>(Comparator.reverseOrder());
        var partitioned = Arrays.stream(numbers).boxed().collect(partitioningBy(
                OddAndEven::isEven,
                toCollection(collectionFactory)));
        return new OddAndEven(partitioned.get(Boolean.TRUE), partitioned.get(Boolean.FALSE));
    }

    private static boolean isEven(int number) {
        return number % 2 == 0;
    }

    public SortedSet<Integer> getEvenNumbers() {
        return evenNumbers;
    }

    public SortedSet<Integer> getOddNumbers() {
        return oddNumbers;
    }
}
