package com.alevel.java.nix.homework.lesson10.task2;

public class MedianOfTwoSortedArrays {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int length1 = nums1.length, length2 = nums2.length;
        // return median of nums2 - constant-time fast-path
        if (length1 == 0) {
            return median(nums2, 0, length2);
        }
        // return median of nums1 - constant-time fast-path
        if (length2 == 0) {
            return median(nums1, 0, length1);
        }
        return length1 <= length2
                ? median(nums1, nums2, length1, length2)
                : median(nums2, nums1, length2, length1);
    }

    private static double median(int[] nums1, int[] nums2, int length1, int length2) {
        int last1 = nums1[length1 - 1];
        int first2 = nums2[0];
        if (last1 <= first2) {
            int length = length2 - length1;
            if (length == 0) {
                return (last1 + first2) / 2.0;
            }
            return median(nums2, 0, length);
        }
        int first1 = nums1[0];
        int last2 = nums2[length2 - 1];
        if (last2 <= first1) {
            int length = length2 - length1;
            if (length == 0) {
                return (last2 + first1) / 2.0;
            }
            return median(nums2, length1, length);
        }

        return search(nums1, nums2, length1, length2);
    }

    private static double search(int[] nums1, int[] nums2, int length1, int length2) {
        int min = 0, max = length1, total = length1 + length2, mid = (total + 1) >> 1;

        do {
            int partition1 = (min + max) >> 1;
            int partition2 = mid - partition1;

            int left1 = partition1 > 0 ? nums1[partition1 - 1] : Integer.MIN_VALUE;
            int right2 = partition2 < length2 ? nums2[partition2] : Integer.MAX_VALUE;

            if (left1 <= right2) {
                int left2 = partition2 > 0 ? nums2[partition2 - 1] : Integer.MIN_VALUE;
                int right1 = partition1 < length1 ? nums1[partition1] : Integer.MAX_VALUE;

                if (left2 <= right1) { // median found
                    return total % 2 == 0
                            ? (Math.max(left1, left2) + Math.min(right1, right2)) / 2.0
                            : Math.max(left1, left2);
                }

                min = partition1 + 1;
            } else {
                max = partition1 - 1;
            }

        } while (min <= max);

        return -1.0;
    }

    private static double median(int[] nums, int from, int length) {
        int mid = length >> 1;
        return length % 2 == 0
                ? (nums[from + mid] + nums[from + mid - 1]) / 2.0
                : nums[from + mid];
    }

}
