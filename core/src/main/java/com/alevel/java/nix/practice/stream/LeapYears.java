package com.alevel.java.nix.practice.stream;

import java.time.Year;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class LeapYears {

    public List<Year> find(Collection<Year> years) {

        return years.stream()
                .filter(Year::isLeap)
                .sorted()
                .collect(Collectors.toList());
    }
}
