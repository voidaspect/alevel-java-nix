package com.alevel.java.nix.homework.lesson10.task1;

public class LongestCommonPrefix {

    public String longestCommonPrefix(String[] strings) {
        int length = strings.length;
        if (length == 0) return "";

        String first = strings[0];

        int firstLength = first.length();
        int prefixLength = 0;

        for (int charIndex = 0; charIndex < firstLength; charIndex++) {
            char current = first.charAt(charIndex);
            for (int stringIndex = 1; stringIndex < length; stringIndex++) {
                String string = strings[stringIndex];
                if (charIndex == string.length() || current != string.charAt(charIndex)) {
                    return first.substring(0, prefixLength);
                }
            }
            prefixLength++;
        }
        return first;
    }

}
