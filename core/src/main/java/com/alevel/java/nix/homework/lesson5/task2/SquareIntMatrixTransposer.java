package com.alevel.java.nix.homework.lesson5.task2;

public final class SquareIntMatrixTransposer extends AbstractIntMatrixTransposer {

    @Override
    int[][] transpose(int[][] matrix, int rows) {
        for (int i = 0, outerLoopSize = rows - 1; i < outerLoopSize; i++) {
            for (int j = i + 1; j < rows; j++) {
                // swap
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tmp;
            }
        }
        return matrix;
    }

}
