package com.alevel.java.nix.homework.lesson4.task1;

import com.alevel.java.nix.homework.lesson4.print.PrintableTextResult;

public final class DigitFizzBuzz extends PrintableTextResult {

    private static final String FIZZ = "fizz";

    private static final String BUZZ = "buzz";

    private static final String FIZZ_BUZZ = "fizzbuzz";

    private static final String NEW_LINE = System.lineSeparator();

    private final String text;

    private final int source;

    private DigitFizzBuzz(String text, int source) {
        this.text = text;
        this.source = source;
    }

    @Override
    public String getText() {
        return text;
    }

    public int getSource() {
        return source;
    }

    //region construct fizzbuzz output

    public static DigitFizzBuzz rightToLeft(final int number) {
        int digits = Math.abs(number);
        if (digits < 10) {
            return singleDigit(digits, number);
        }
        var text = new StringBuilder();
        for (; digits > 0; digits /= 10) {
            int digit = digits % 10;
            appendDigit(text, digit);
        }
        return new DigitFizzBuzz(text.toString(), number);
    }

    public static DigitFizzBuzz leftToRight(final int number) {
        int digits = Math.abs(number);
        if (digits < 10) {
            return singleDigit(digits, number);
        }
        var text = new StringBuilder();
        for (int powerOf10 = findClosestPowerOf10From1To9(digits); powerOf10 >= 0; powerOf10--) {
            int divisor = POWERS_OF_10[powerOf10];
            int digit = digits / divisor;
            appendDigit(text, digit);
            digits %= divisor;
        }
        return new DigitFizzBuzz(text.toString(), number);
    }

    /**
     * Table for fizz-buzz strings for single-digit numbers 0 - 9
     * numbers, divisible by 2 are "fizz"
     * numbers, divisible by 3 are "buzz",
     * numbers, divisible by 2 and 3 (which are 0 and 6) are "fizzbuzz"
     * all other numbers are simply converted to their string form
     */
    private static final String[] STRINGS_FOR_DIGITS = {
            FIZZ_BUZZ,
            "1",
            FIZZ,
            BUZZ,
            FIZZ,
            "5",
            FIZZ_BUZZ,
            "7",
            FIZZ,
            BUZZ
    };

    private static DigitFizzBuzz singleDigit(int digit, int source) {
        return new DigitFizzBuzz(STRINGS_FOR_DIGITS[digit] + NEW_LINE, source);
    }

    private static void appendDigit(StringBuilder text, int digit) {
        text.append(STRINGS_FOR_DIGITS[digit]).append(NEW_LINE);
    }

    //endregion

    //region closest power of 10

    /**
     * Powers of 10 that are in range of Java int type [10^1, 10^9]
     */
    private static final int[] POWERS_OF_10 = {
            1,
            10,
            100,
            1000,
            10000,
            100000,
            1000000,
            10000000,
            100000000,
            1000000000
    };

    /**
     * Uses binary search to find closest power of 10 smaller than the given number
     *
     * @param number positive integer equal or greater than 10
     * @return power of 10 from 1 to 9
     */
    private static int findClosestPowerOf10From1To9(int number) {
        // binary search
        if (number < POWERS_OF_10[5]) {
            if (number < POWERS_OF_10[3]) {
                if (number < POWERS_OF_10[2]) {
                    return 1; // 10
                }
                return 2; // 100
            }
            if (number < POWERS_OF_10[4]) {
                return 3; // 1 000
            }
            return 4; // 10 000
        }
        if (number < POWERS_OF_10[8]) {
            if (number < POWERS_OF_10[7]) {
                if (number < POWERS_OF_10[6]) {
                    return 5; // 100 000
                }
                return 7; // 10 000 000
            }
            return 6; // 1 000 000
        }
        if (number < POWERS_OF_10[9]) {
            return 8; // 100 000 000
        }
        return 9; // 1 000 000 000
    }

    //endregion

}
