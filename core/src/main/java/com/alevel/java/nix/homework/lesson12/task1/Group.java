package com.alevel.java.nix.homework.lesson12.task1;

import java.util.*;

public final class Group implements HumanReadable {

    private final List<Student> students;

    private final ContractStudents contractStudents;

    public Group() {
        students = new ArrayList<>();
        contractStudents = new ContractStudents();
    }

    public List<Student> getStudents() {
        return Collections.unmodifiableList(students);
    }

    public void add(Student student) {
        if (student != null) {
            students.add(student);
            contractStudents.addIfOnContract(student);
        }
    }

    public void add(Student... students) {
        addAll(Arrays.asList(students));
    }

    public void add(Iterable<Student> students) {
        if (students == null) return;
        addAll(students);
    }

    private void addAll(Iterable<Student> students) {
        for (Student student : students) {
            add(student);
        }
    }

    public ContractStudents getContractStudents() {
        return contractStudents;
    }

    public static final class ContractStudents implements HumanReadable {

        private double totalCost;

        private final List<ContractStudent> list;

        private ContractStudents() {
            list = new ArrayList<>();
        }

        private void addIfOnContract(Student student) {
            if (student instanceof ContractStudent) {
                var contractStudent = (ContractStudent) student;
                list.add(contractStudent);
                totalCost += contractStudent.getContractCost();
            }
        }

        public List<ContractStudent> list() {
            return Collections.unmodifiableList(list);
        }

        public double getTotalCost() {
            return totalCost;
        }

        @Override
        public String toHumanReadableText() {
            return Group.toHumanReadableText(list);
        }
    }

    @Override
    public String toHumanReadableText() {
        return toHumanReadableText(students);
    }

    private static String toHumanReadableText(List<? extends Student> students) {
        var joiner = new StringJoiner(System.lineSeparator());
        for (Student student : students) {
            joiner.add(student.toHumanReadableText());
        }
        return joiner.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return students.equals(group.students);
    }

    @Override
    public int hashCode() {
        return students.hashCode();
    }

    @Override
    public String toString() {
        return "Group{students=" + students + '}';
    }

}
