package com.alevel.java.nix.practice.stream;

import java.util.Collection;
import java.util.function.UnaryOperator;

public final class UnaryOperatorChain<T> implements UnaryOperator<T> {

    private final UnaryOperator<T> combined;

    private UnaryOperatorChain(UnaryOperator<T> combined) {
        this.combined = combined;
    }

    public static <T> UnaryOperatorChain<T> fromCollection(Collection<UnaryOperator<T>> operators) {
        var combined = operators.stream()
                .reduce((current, next) -> e -> next.apply(current.apply(e)))
                .orElseGet(UnaryOperator::identity);

        return new UnaryOperatorChain<>(combined);
    }

    @Override
    public T apply(T t) {
        return combined.apply(t);
    }
}
