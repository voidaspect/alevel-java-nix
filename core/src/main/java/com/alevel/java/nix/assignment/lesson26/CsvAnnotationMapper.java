package com.alevel.java.nix.assignment.lesson26;

import com.alevel.java.nix.homework.lesson19.CsvTable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.entry;

public class CsvAnnotationMapper {

    @FunctionalInterface
    private interface Setter {
        void set(Object target, Field field, String property) throws IllegalAccessException;
    }

    private static final Map<Class<?>, Setter> SETTERS = Map.ofEntries(
            entry(String.class, (target, field, property) -> field.set(target, property)),

            entry(int.class, (target, field, property) -> field.setInt(target, Integer.parseInt(property))),
            entry(Integer.class, (target, field, property) -> field.set(target, Integer.valueOf(property))),

            entry(long.class, (target, field, property) -> field.setLong(target, Long.parseLong(property))),
            entry(Long.class, (target, field, property) -> field.set(target, Long.valueOf(property))),

            entry(double.class, (target, field, property) -> field.setDouble(target, Double.parseDouble(property))),
            entry(Double.class, (target, field, property) -> field.set(target, Double.valueOf(property))),

            entry(boolean.class, (target, field, property) -> field.setBoolean(target, Boolean.parseBoolean(property))),
            entry(Boolean.class, (target, field, property) -> field.set(target, Boolean.valueOf(property)))
    );

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static final Setter ENUM_SETTER = (target, field, property) -> field.set(
            target, Enum.valueOf((Class<Enum>) field.getType(), property.toUpperCase()));

    private static final Setter NOOP_SETTER = (target, field, property) -> {};

    public <T> List<T> map(CsvTable table, Class<T> c) {
        int height = table.height();
        List<T> elements = new ArrayList<>(height);

        Constructor<T> constructor;
        try {
            constructor = c.getConstructor();
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        List<Field> fields = Arrays.stream(c.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(CsvColumn.class))
                .filter(Field::trySetAccessible)
                .collect(Collectors.toList());

        for (int row = 0; row < height; row++) {
            T element;

            try {
                element = constructor.newInstance();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }

            for (Field field : fields) {
                Setter setter = field.getType().isEnum()
                        ? ENUM_SETTER
                        : SETTERS.getOrDefault(field.getType(), NOOP_SETTER);
                String header = field.getAnnotation(CsvColumn.class).value();
                String value = table.get(row, header);
                try {
                    setter.set(element, field, value);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }

            elements.add(element);
        }

        return elements;
    }
}
