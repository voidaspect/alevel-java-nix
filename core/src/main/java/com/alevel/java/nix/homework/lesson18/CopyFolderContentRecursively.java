package com.alevel.java.nix.homework.lesson18;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class CopyFolderContentRecursively {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Too few arguments");
            return;
        }
        Path from = Paths.get(args[0]);
        Path to = Paths.get(args[1]);
        try {
            new CopyFolderContentRecursively().copy(from, to);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void copy(Path from, Path to) throws IOException {
        if (Files.isRegularFile(to)) {
            System.err.println(to + " is not a directory");
        }
        Files.walk(from).forEach(source -> cp(source, to.resolve(from.relativize(source))));
    }

    private void cp(Path from, Path to) {
        try {
            Files.copy(from, to, REPLACE_EXISTING);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
