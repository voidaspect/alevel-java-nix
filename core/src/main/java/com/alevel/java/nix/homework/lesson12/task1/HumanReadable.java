package com.alevel.java.nix.homework.lesson12.task1;

public interface HumanReadable {

    String toHumanReadableText();

}
