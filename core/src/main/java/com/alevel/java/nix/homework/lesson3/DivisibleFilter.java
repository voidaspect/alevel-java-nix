package com.alevel.java.nix.homework.lesson3;

import java.util.Arrays;

public final class DivisibleFilter {

    private final int divisor;

    public DivisibleFilter(int divisor) {
        if (divisor == 0) {
            throw new IllegalArgumentException("Divisor can't be 0!");
        }
        this.divisor = divisor;
    }

    public int[] filter(int... values) {
        int length = values.length;
        if (length == 0) {
            return values;
        }
        if (divisor == 1) {
            return Arrays.copyOf(values, length);
        }
        int[] tmp = new int[length];
        int quantity = 0;
        for (int element : values) {
            if (element % divisor == 0) {
                tmp[quantity++] = element;
            }
        }
        return Arrays.copyOf(tmp, quantity);
    }

    //region boilerplate

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DivisibleFilter)) return false;

        DivisibleFilter that = (DivisibleFilter) o;

        return divisor == that.divisor;
    }

    @Override
    public int hashCode() {
        return divisor;
    }

    //endregion
}
