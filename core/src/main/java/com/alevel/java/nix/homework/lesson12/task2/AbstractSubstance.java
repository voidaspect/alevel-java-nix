package com.alevel.java.nix.homework.lesson12.task2;

public abstract class AbstractSubstance implements Substance {

    private double temperature = ROOM_TEMPERATURE;

    @Override
    public final void heatUp(double t) {
        temperature = Math.max(temperature + t, ABSOLUTE_ZERO_CELSIUS);
    }

    @Override
    public final double getTemperature() {
        return temperature;
    }

    @Override
    public final State getState() {
        final State state;
        if (temperature > getBoilingPoint()) {
            state = State.GAS;
        } else if (temperature > getMeltingPoint()) {
            state = State.LIQUID;
        } else {
            state = State.SOLID;
        }
        return state;
    }

    protected abstract double getMeltingPoint();

    protected abstract double getBoilingPoint();

}
