package com.alevel.java.nix.homework.lesson25;

public class AppProperties {

    @PropertyKey("client.url")
    public String url;

    @PropertyKey("client.timeout")
    public int timeout;

    @PropertyKey("client.retry")
    public boolean retry;

}
