package com.alevel.java.nix.practice.huffman;

import java.util.*;
import java.util.stream.Collectors;

public final class HuffmanCodec {

    public static void main(String[] args) {
        var tree = encode("I have some string for you to encode and compress");
        var output = tree.encodeAsText();

        System.out.println(output);

        var queue = new ArrayDeque<Boolean>();

        for (int i = 0; i < output.length(); i++) {
            queue.add(output.charAt(i) == '1');
        }

        System.out.println(tree.decode(queue));
    }

    public static HuffmanEncoder encode(String text) {
        if (text.isEmpty()) return EMPTY_ENCODER;

        var frequency = frequency(text);

        PriorityQueue<HuffmanNode> pq = frequency.entrySet().stream()
                .map(entry -> new HuffmanLeaf(entry.getValue(), entry.getKey()))
                .collect(Collectors.toCollection(PriorityQueue::new));

        while (pq.size() != 1) {
            var right = pq.poll();
            var left = pq.poll();

            pq.add(new HuffmanBranch(left, right));
        }

        var root = pq.poll();

        return new HuffmanTree(root, text);
    }

    private static Map<Character, Integer> frequency(String text) {
        Map<Character, Integer> frequency = new HashMap<>();
        for (int i = 0, length = text.length(); i < length; i++) {
            frequency.merge(text.charAt(i), 1, Integer::sum);
        }
        return frequency;
    }

    public interface HuffmanEncoder {

        String encodeAsText();

        String decode(Queue<Boolean> bits);

        Map<Character, Code> getCodes();
    }

    private static final HuffmanEncoder EMPTY_ENCODER = new HuffmanEncoder() {
        @Override
        public String encodeAsText() {
            return "";
        }

        @Override
        public String decode(Queue<Boolean> bits) {
            return "";
        }

        @Override
        public Map<Character, Code> getCodes() {
            return Map.of();
        }
    };

    private static final class HuffmanTree implements HuffmanEncoder {

        private final String text;

        private final Map<Character, Code> codes;

        private final HuffmanNode root;

        private HuffmanTree(HuffmanNode root, String text) {
            this.root = root;
            this.text = text;
            this.codes = Collections.unmodifiableMap(root.encode());
        }

        @Override
        public String encodeAsText() {
            var sb = new StringBuilder();
            for (int i = 0, length = text.length(); i < length; i++) {
                var code = codes.get(text.charAt(i));
                sb.append(code);
            }
            return sb.toString();
        }

        @Override
        public String decode(Queue<Boolean> bits) {
            var sb = new StringBuilder();
            while (!bits.isEmpty()) {
                char next = root.decode(bits);
                sb.append(next);
            }
            return sb.toString();
        }

        @Override
        public Map<Character, Code> getCodes() {
            return codes;
        }
    }

    public static final class Code {

        private final long bits;

        private final int length;

        private String stringValue;

        public Code(long bits, int length) {
            this.bits = bits;
            this.length = length;
        }

        public long getBits() {
            return bits;
        }

        public int getLength() {
            return length;
        }

        @Override
        public String toString() {
            if (stringValue == null) {
                var stripped = Long.toBinaryString(bits);
                int padding = length - stripped.length();
                stringValue = padding == 0 ? stripped : "0".repeat(padding) + stripped;
            }
            return stringValue;
        }
    }

    private static abstract class HuffmanNode implements Comparable<HuffmanNode> {

        final int frequency;

        protected HuffmanNode(int frequency) {
            this.frequency = frequency;
        }

        @Override
        public int compareTo(HuffmanNode o) {
            return Integer.compare(frequency, o.frequency);
        }

        final Map<Character, Code> encode() {
            Map<Character, Code> codes = new HashMap<>();
            encode(codes, 0, 0);
            return codes;
        }

        abstract void encode(Map<Character, Code> codes, int depth, long bits);

        abstract char decode(Queue<Boolean> bits);

        static int frequency(HuffmanNode node) {
            return node != null ? node.frequency : 0;
        }
    }

    private static final class HuffmanBranch extends HuffmanNode {

        final HuffmanNode left;

        final HuffmanNode right;

        private HuffmanBranch(HuffmanNode left, HuffmanNode right) {
            super(frequency(left) + frequency(right));
            this.left = left;
            this.right = right;
        }

        @Override
        void encode(Map<Character, Code> codes, int depth, long bits) {
            if (left != null) {
                left.encode(codes, depth + 1, bits << 1);
            }
            if (right != null) {
                right.encode(codes, depth + 1, (bits << 1) | 1);
            }
        }

        @Override
        char decode(Queue<Boolean> bits) {
            boolean bit = bits.remove();
            return bit ? right.decode(bits) : left.decode(bits) ;
        }
    }

    private static final class HuffmanLeaf extends HuffmanNode {

        final char value;

        private HuffmanLeaf(int frequency, char value) {
            super(frequency);
            this.value = value;
        }

        @Override
        void encode(Map<Character, Code> codes, int depth, long bits) {
            codes.put(value, new Code(bits, depth));
        }

        @Override
        char decode(Queue<Boolean> bits) {
            return value;
        }
    }

}
