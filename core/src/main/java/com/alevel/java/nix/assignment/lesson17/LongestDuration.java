package com.alevel.java.nix.assignment.lesson17;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;

public final class LongestDuration {

    public Duration find(Collection<LocalDateTime> timestamps) {

        return timestamps.stream()
                .map(Range::empty)
                .reduce(Range::merge)
                .map(range -> Duration.between(range.getStart(), range.getEnd()))
                .orElse(Duration.ZERO);
    }

}
