package com.alevel.java.nix.homework.lesson16.task1;

import java.util.Collection;

public final class StringCollectionToNumberParser {

    public long parse(Collection<String> strings) {

        return strings.stream()
                .flatMapToInt(String::codePoints)
                .filter(Character::isDigit)
                .map(codepoint -> Character.digit(codepoint, 10))
                .asLongStream()
                .reduce((num, digit) -> num * 10 + digit)
                .orElseThrow(() -> new IllegalArgumentException("Input contains no valid digits: " + strings));
    }

}
