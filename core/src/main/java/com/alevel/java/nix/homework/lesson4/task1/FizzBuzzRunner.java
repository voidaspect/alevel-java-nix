package com.alevel.java.nix.homework.lesson4.task1;

import java.util.Scanner;

public class FizzBuzzRunner {

    public static void main(String[] args) {
        final int number;

        if (args.length > 0) {
            number = Integer.parseInt(args[0]);
        } else {
            System.out.println("Please, enter an integer");
            var scanner = new Scanner(System.in);
            number = scanner.nextInt();
        }

        var fizzBuzz = DigitFizzBuzz.leftToRight(number);

        System.out.println("Left-to-right decimal-digit FizzBuzz of " + number);

        fizzBuzz.print();
    }

}
