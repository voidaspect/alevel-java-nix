package com.alevel.java.nix.homework.lesson14;

@FunctionalInterface
public interface Block<T> {

    T run() throws Exception;

}
