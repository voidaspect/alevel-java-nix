package com.alevel.java.nix.homework.lesson12.task2;

import java.util.Scanner;

public class SubstanceTempSimulationConsoleRunner {

    public static void main(String[] args) {

        var newline = System.lineSeparator();

        final String chooseSubstancePrompt = "Please, choose a substance:" + newline +
                "1. Oxygen" + newline +
                "2. Water" + newline +
                "3. Iron" + newline;

        var scanner = new Scanner(System.in);

        Substance substance = null;

        do {
            System.out.print(chooseSubstancePrompt);
            switch (scanner.nextInt()) {
                case 1:
                    substance = new Oxygen();
                    break;
                case 2:
                    substance = new Water();
                    break;
                case 3:
                    substance = new Iron();
                    break;
            }
        } while (substance == null);

        System.out.print("Temperature is " + substance.getTemperature() +
                " state is " + substance.getState() + newline);

        final String changeTemperaturePrompt = "Please, enter the amount in degrees Celsius " +
                "by which to change the temperature of the substance" + newline;

        double deltaT;

        while (true) {
            System.out.print(changeTemperaturePrompt);
            deltaT = scanner.nextDouble();

            if (Double.compare(deltaT, 0.0) == 0) break;

            double before = substance.getTemperature();
            substance.heatUp(deltaT);
            double after = substance.getTemperature();
            State stateAfter = substance.getState();

            System.out.printf("Temperature changed from %.3f C to %.3f C, state is now %s%n", before, after, stateAfter);
        }
    }

}
