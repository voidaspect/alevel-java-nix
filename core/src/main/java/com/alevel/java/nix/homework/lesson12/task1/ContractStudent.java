package com.alevel.java.nix.homework.lesson12.task1;

public class ContractStudent extends Student {

    private double contractCost;

    public ContractStudent(int age, String name, double contractCost) {
        super(age, name);
        setContractCost(contractCost);
    }

    public double getContractCost() {
        return contractCost;
    }

    public void setContractCost(double contractCost) {
        if (contractCost < 0.0) {
            throw new IllegalArgumentException("Contract cost should not be negative, got: " + contractCost);
        }
        this.contractCost = contractCost;
    }

    @Override
    public String toHumanReadableText() {
        return String.format("%s, contract cost: %.2f", super.toHumanReadableText(), contractCost);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!getClass().equals(o.getClass())) return false;

        ContractStudent that = (ContractStudent) o;

        return getAge() == that.getAge()
                && Double.compare(that.contractCost, contractCost) == 0
                && getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Double.hashCode(contractCost);
        return result;
    }

    @Override
    public String toString() {
        return "ContractStudent{" +
                "age=" + getAge() +
                ", name='" + getName() + '\'' +
                ", contractCost=" + contractCost +
                '}';
    }
}
