package com.alevel.java.nix.homework.lesson9.model;

import com.alevel.java.nix.homework.lesson9.view.TurnResultView;

public interface TicTacToe {

    void reset();

    TurnResultView mark(int row, int col);

    Player getCurrentPlayer();

    TurnResultView view();

}
