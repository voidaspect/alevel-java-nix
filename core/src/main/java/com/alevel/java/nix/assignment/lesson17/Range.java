package com.alevel.java.nix.assignment.lesson17;

public final class Range<T extends Comparable<? super T>> {

    private final T start;

    private final T end;

    private Range(T start, T end) {
        this.start = start;
        this.end = end;
    }

    public T getStart() {
        return start;
    }

    public T getEnd() {
        return end;
    }

    static <T extends Comparable<? super T>> Range<T> of(T first, T second) {
        return first.compareTo(second) < 0 ? new Range<>(first, second) : new Range<>(second, first);
    }

    static <T extends Comparable<? super T>> Range<T> empty(T value) {
        return new Range<>(value, value);
    }

    static <T extends Comparable<? super T>> Range<T> merge(Range<T> first, Range<T> second) {
        if (first.start.compareTo(second.start) < 0) {
            if (first.end.compareTo(second.end) > 0) return first;
            return new Range<>(first.start, second.end);
        } else {
            if (first.end.compareTo(second.end) < 0) return second;
            return new Range<>(second.start, first.end);
        }
    }
}
