package com.alevel.java.nix.practice.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;

public class CustomCollector {
    public static void main(String[] args) {

        Collector<String, List<Integer>, List<Integer>> toListOfLengths = Collector.of(
                ArrayList::new,
                (list, string) -> list.add(string.length()),
                (list1, list2) -> {
                    list1.addAll(list2);
                    return list1;
                },
                Collections::unmodifiableList

        );
        List<Integer> lengths = List.of("abc", "abcd", "abcde").stream().collect(toListOfLengths);
        System.out.println(lengths);
    }
}
