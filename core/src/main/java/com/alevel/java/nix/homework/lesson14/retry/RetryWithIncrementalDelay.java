package com.alevel.java.nix.homework.lesson14.retry;

import com.alevel.java.nix.homework.lesson14.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class RetryWithIncrementalDelay<T> extends Retry<T> {

    private static final Logger log = LoggerFactory.getLogger(RetryWithIncrementalDelay.class);

    private final long initialDelay;

    RetryWithIncrementalDelay(int times, long initialDelay, Block<T> block) {
        super(times, block);
        this.initialDelay = initialDelay;
    }

    @Override
    void backoff(int attempt) throws InterruptedException {
        long millis = initialDelay * attempt;
        log.warn("Waiting {} ms before retry", millis);
        Thread.sleep(millis);
    }
}
