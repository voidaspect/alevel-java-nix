package com.alevel.java.nix.homework.lesson12.task2;

public final class Oxygen extends AbstractSubstance {

    @Override
    protected double getMeltingPoint() {
        return -218.79;
    }

    @Override
    protected double getBoilingPoint() {
        return -182.962;
    }

}
