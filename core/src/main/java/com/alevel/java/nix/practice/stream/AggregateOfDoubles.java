package com.alevel.java.nix.practice.stream;

import java.util.Optional;
import java.util.stream.DoubleStream;

public final class AggregateOfDoubles {

    private final double max;

    private final double min;

    private final double average;

    private final double sum;

    private AggregateOfDoubles(double max, double min, double average, double sum) {
        this.max = max;
        this.min = min;
        this.average = average;
        this.sum = sum;
    }

    public static Optional<AggregateOfDoubles> summarize(double... doubles) {
        if (doubles.length == 0) return Optional.empty();

        var summary = DoubleStream.of(doubles).summaryStatistics();

        return Optional.of(new AggregateOfDoubles(
                summary.getMax(),
                summary.getMin(),
                summary.getAverage(),
                summary.getSum()));
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public double getAverage() {
        return average;
    }

    public double getSum() {
        return sum;
    }
}
