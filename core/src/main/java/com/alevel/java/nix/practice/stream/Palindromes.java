package com.alevel.java.nix.practice.stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class Palindromes {

    private static final Pattern WHITESPACE = Pattern.compile("\\s+");

    public List<String> find(Collection<String> strings) {

        return strings.stream()
                .flatMap(string -> Arrays.stream(WHITESPACE.split(string)))
                .filter(Palindromes::isPalindrome)
                .collect(Collectors.toList());
    }

    private static boolean isPalindrome(String string) {
        int start = 0;
        int end = string.length() - 1;

        while (start < end) {
            if (string.charAt(start++) != string.charAt(end--)) return false;
        }
        return true;
    }

}
