package com.alevel.java.nix.homework.lesson6.task2;

public final class ZigZagConversion {

    public String convert(String s, int rows) {
        int length = s.length();
        if (length <= rows || rows < 2) {
            return s;
        }
        var result = new StringBuilder(length);
        int zigzag = (rows << 1) - 2;
        int cursor;
        for (cursor = 0; cursor < length; cursor += zigzag) {
            result.append(s.charAt(cursor));
        }
        int lastRow = rows - 1;
        for (int row = 1; row < lastRow; row++) {
            int zig = (rows - (row + 1)) << 1;
            int zag = row << 1;
            boolean flag;
            for (cursor = row, flag = true; cursor < length; flag = !flag) {
                result.append(s.charAt(cursor));
                cursor += flag ? zig : zag;
            }
        }
        for (cursor = lastRow; cursor < length; cursor += zigzag) {
            result.append(s.charAt(cursor));
        }
        return result.toString();
    }

}
