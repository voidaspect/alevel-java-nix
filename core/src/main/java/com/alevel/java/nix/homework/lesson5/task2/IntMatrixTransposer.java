package com.alevel.java.nix.homework.lesson5.task2;

public interface IntMatrixTransposer {

    int[][] transpose(int[][] matrix);

}