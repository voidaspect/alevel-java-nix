package com.alevel.java.nix.practice.stream;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class MatchingSubstringRemover {

    private final Pattern pattern;

    public MatchingSubstringRemover(Pattern pattern) {
        this.pattern = pattern;
    }

    public List<String> removeAllMatching(Collection<String> input) {

        return input.stream()
                .map(string -> pattern.matcher(string).replaceAll(""))
                .collect(Collectors.toList());
    }
}
