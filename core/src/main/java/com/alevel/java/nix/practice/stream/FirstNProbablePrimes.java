package com.alevel.java.nix.practice.stream;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FirstNProbablePrimes {

    public List<BigInteger> find(int n) {

        return Stream.iterate(BigInteger.TWO, BigInteger::nextProbablePrime)
                .limit(n)
                .collect(Collectors.toList());
    }

}
