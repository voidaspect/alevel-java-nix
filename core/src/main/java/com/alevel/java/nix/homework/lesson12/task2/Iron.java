package com.alevel.java.nix.homework.lesson12.task2;

public final class Iron extends AbstractSubstance {

    @Override
    protected double getMeltingPoint() {
        return 1538;
    }

    @Override
    protected double getBoilingPoint() {
        return 2862;
    }

}
