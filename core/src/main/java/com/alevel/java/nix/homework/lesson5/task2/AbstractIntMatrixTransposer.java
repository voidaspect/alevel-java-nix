package com.alevel.java.nix.homework.lesson5.task2;

abstract class AbstractIntMatrixTransposer implements IntMatrixTransposer {

    @Override
    public int[][] transpose(int[][] matrix) {
        int rows = matrix.length;
        if (rows == 0) {
            return matrix;
        }
        return transpose(matrix, rows);
    }

    abstract int[][] transpose(int[][] matrix, int rows);

}
