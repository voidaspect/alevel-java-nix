package com.alevel.java.nix.homework.lesson32;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static java.util.function.Predicate.isEqual;
import static java.util.function.Predicate.not;

public class MessageConsumer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(MessageConsumer.class);

    private final ConcurrentBox<String> input;

    private final Path path;

    public MessageConsumer(ConcurrentBox<String> input, Path path) {
        this.input = input;
        this.path = path;
    }

    @Override
    public void run() {
        String message = null;

        try (var output = new RandomAccessFile(path.toFile(), "rw")) {
            while (!input.isDisposed()) {
                Optional<String> next = input.get().filter(not(isEqual(message)));
                if (next.isPresent()) {
                    message = next.get();
                    output.setLength(0L);
                    output.writeBytes(message);
                }
                if (input.isDisposed()) break; // don't sleep if the box is closed and empty
                TimeUnit.SECONDS.sleep(1);
            }
        } catch (IOException e) {
            log.error("Failed to write message to file", e);
        } catch (InterruptedException e) {
            log.error("Message consumer loop interrupted", e);
            Thread.currentThread().interrupt();
        }
    }

}
