package com.alevel.java.nix.homework.lesson3;

public final class BubbleSort {

    public void sort(int[] array) {
        int length = array.length;
        for (int i = 0; i < length; i++) {
            int outer = array[i];
            for (int j = i + 1; j < length; j++) {
                int inner = array[j];
                if (inner < outer) {
                    array[i] = inner;
                    array[j] = outer;
                    outer = inner;
                }
            }
        }
    }

}
