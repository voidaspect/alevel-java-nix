package com.alevel.java.nix.assignment.lesson17;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class Lesson17Assignment {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        List<LocalDateTime> timestamps = Arrays.asList(
                now,
                now.minusHours(1),
                now.plusHours(1),
                now.minusDays(1),
                now.plusDays(1),
                now.plusHours(25),
                now.plusDays(2)
        );
        System.out.println(timestamps);

        var byDate = new TimestampsByDate().group(timestamps);

        String newline = System.lineSeparator();
        String humanReadableOutput = byDate.entrySet().stream()
                .map(String::valueOf)
                .collect(joining(newline, "Human readable output:" + newline, newline));
        System.out.print(humanReadableOutput);

        var longestDuration = new LongestDuration().find(timestamps);
        System.out.println(longestDuration.toHours());
    }

}
